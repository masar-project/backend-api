<?php

use App\Http\Controllers\DashboardEmployee\CompanyProfileController;
use App\Http\Controllers\DashboardEmployee\CustomizedDriverCompanyController;
use App\Http\Controllers\DashboardEmployee\CustomizedPointCompanyController;
use App\Http\Controllers\DashboardEmployee\DefaultTripCompanyController;
use App\Http\Controllers\DashboardEmployee\DriverCompanyController;
use App\Http\Controllers\DashboardEmployee\EmployeeCompanyController;
use App\Http\Controllers\DashboardEmployee\EmployeeProfileController;
use App\Http\Controllers\DashboardEmployee\PointOfSaleCompanyController;
use App\Http\Controllers\DashboardEmployee\ProductCompanyController;
use App\Http\Controllers\DashboardEmployee\TripCompanyController;
use App\Http\Controllers\MediaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix("my-account")->group(function () {
    Route::get('/', [EmployeeProfileController::class, 'showMyAccount'])->name('media.upload');
    Route::put('edit-profile-img', [EmployeeProfileController::class, 'editProfileImg'])->name('media.upload');
    Route::put('edit-profile', [EmployeeProfileController::class, 'editProfile'])->name('media.upload');
    Route::post('change-password', [EmployeeProfileController::class, 'todo'])->name('media.upload');
});

Route::prefix("company/{company}")->group(function () {

    Route::prefix("profile")->group(function () {
        Route::get('/', [CompanyProfileController::class, 'showCompanyDetails'])->name('media.upload');
        Route::put('/edit-img', [CompanyProfileController::class, 'editProfileImg'])->name('media.upload');
        Route::put('/edit-details', [CompanyProfileController::class, 'editProfile'])->name('media.upload');
        Route::put('/edit-categories', [CompanyProfileController::class, 'editCategories'])->name('media.upload');
    });

    Route::prefix("employees")->group(function () {
        Route::get('/', [EmployeeCompanyController::class, 'index'])->name('media.upload');
        Route::post('/', [EmployeeCompanyController::class, 'store'])->name('media.upload');
        Route::get('/{employee}', [EmployeeCompanyController::class, 'show'])->name('media.upload');
        Route::put('/{employee}', [EmployeeCompanyController::class, 'update'])->name('media.upload');
        Route::post('/{employee}/disable', [EmployeeCompanyController::class, 'disable'])->name('media.upload');
        Route::post('/{employee}/enable', [EmployeeCompanyController::class, 'enable'])->name('media.upload');
    });

    Route::prefix("drivers")->group(function () {
        Route::get('/', [DriverCompanyController::class, 'index'])->name('media.upload');
        Route::post('/', [DriverCompanyController::class, 'store'])->name('media.upload');
        Route::get('/{driver}', [DriverCompanyController::class, 'show'])->name('media.upload');
        Route::put('/{driver}', [DriverCompanyController::class, 'update'])->name('media.upload');
        Route::post('/{driver}/disable', [DriverCompanyController::class, 'disable'])->name('media.upload');
        Route::post('/{driver}/enable', [DriverCompanyController::class, 'enable'])->name('media.upload');
    });

    Route::prefix("default-trips")->group(function () {
        Route::get('/', [DefaultTripCompanyController::class, 'index'])->name('media.upload');
    Route::post('/', [DefaultTripCompanyController::class, 'store'])->name('media.upload');
        Route::get('/{defaultTrip}', [DefaultTripCompanyController::class, 'show'])->name('media.upload');
        Route::put('/{defaultTrip}', [DefaultTripCompanyController::class, 'update'])->name('media.upload');
        Route::delete('/{defaultTrip}', [DefaultTripCompanyController::class, 'delete'])->name('media.upload');
        Route::get('{defaultTrip}/preview', [DefaultTripCompanyController::class, 'preview'])->name('media.upload');
        Route::get('{defaultTrip}/generate-trip', [DefaultTripCompanyController::class, 'generateTrip'])->name('media.upload');
    });

    Route::prefix("point-of-sale")->group(function () {
        Route::get('/', [PointOfSaleCompanyController::class, 'index'])->name('media.upload');
        Route::post('/', [PointOfSaleCompanyController::class, 'store'])->name('media.upload');
        Route::get('/{pointOfSale}', [PointOfSaleCompanyController::class, 'show'])->name('media.upload');
        Route::put('/{pointOfSale}', [PointOfSaleCompanyController::class, 'update'])->name('media.upload');
        Route::post('/{pointOfSale}/disable', [PointOfSaleCompanyController::class, 'disable'])->name('media.upload');
        Route::post('/{pointOfSale}/enable', [PointOfSaleCompanyController::class, 'enable'])->name('media.upload');
    });

    Route::prefix("customized-points")->group(function () {
        Route::get('/', [CustomizedPointCompanyController::class, 'index'])->name('media.upload');
        Route::post('/', [CustomizedPointCompanyController::class, 'store'])->name('media.upload');
        Route::get('/{customizedPoint}', [CustomizedPointCompanyController::class, 'show'])->name('media.upload');
        Route::delete('/{customizedPoint}', [CustomizedPointCompanyController::class, 'delete'])->name('media.upload');
    });

    Route::prefix("customized-drivers")->group(function () {
        Route::get('/', [CustomizedDriverCompanyController::class, 'index'])->name('media.upload');
        Route::post('/', [CustomizedDriverCompanyController::class, 'store'])->name('media.upload');
        Route::get('/{customizedDriver}', [CustomizedDriverCompanyController::class, 'show'])->name('media.upload');
        Route::delete('/{customizedDriver}', [CustomizedDriverCompanyController::class, 'delete'])->name('media.upload');
    });


    Route::prefix("trips")->group(function () {
        Route::get('/', [TripCompanyController::class, 'index'])->name('media.upload');
    });


    Route::prefix("products")->group(function () {
        Route::get('/', [ProductCompanyController::class, 'index'])->name('media.upload');
        Route::post('/', [ProductCompanyController::class, 'store'])->name('media.upload');
        Route::put('/{product}', [ProductCompanyController::class, 'update'])->name('media.upload');
        Route::delete('/{product}', [ProductCompanyController::class, 'delete'])->name('media.upload');
    });
});

