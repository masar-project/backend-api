<?php

use App\Http\Controllers\DashboardDriver\DefaultTripDriverController;
use App\Http\Controllers\DashboardDriver\PointOfSaleDriverController;
use App\Http\Controllers\DashboardDriver\TripDriverController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//
//Route::prefix("my-account")->group(function () {
//    Route::get('/', [DriverProfileController::class, 'showMyAccount'])->name('media.upload');
//    Route::put('edit-profile-img', [DriverProfileController::class, 'editProfileImg'])->name('media.upload');
//    Route::put('edit-profile', [DriverProfileController::class, 'editProfile'])->name('media.upload');
//    Route::post('change-password', [DriverProfileController::class, 'todo'])->name('media.upload');
//});


Route::prefix("company/{company}")->group(function () {

    Route::prefix("default-trips")->group(function () {
        Route::get('/', [DefaultTripDriverController::class, 'index'])->name('media.upload');
        Route::get('/{defaultTrip}', [DefaultTripDriverController::class, 'show'])->name('media.upload');
        Route::get('{defaultTrip}/preview', [DefaultTripDriverController::class, 'preview'])->name('media.upload');
    });

    Route::prefix("trips")->group(function () {
        Route::get('/', [TripDriverController::class, 'index'])->name('media.upload');
        Route::get('/current/get', [TripDriverController::class, 'getCurrentTrip'])->name('media.upload');
        Route::prefix('/{trip}')->group(function () {
            Route::get('/', [TripDriverController::class, 'show'])->name('media.upload');
            Route::post('/start', [TripDriverController::class, 'start'])->name('media.upload');
            Route::post('/pause', [TripDriverController::class, 'pause'])->name('media.upload');
            Route::post('/cancel', [TripDriverController::class, 'cancel'])->name('media.upload');
            Route::post('/refresh-solution', [TripDriverController::class, 'refreshSolution'])->name('media.upload');
            Route::prefix('/points-of-sale/{pointOfSale}')->group(function () {
                Route::post('/set-intransit', [PointOfSaleDriverController::class, 'setIntransit'])->name('media.upload');
                Route::post('/set-success', [PointOfSaleDriverController::class, 'setSuccess'])->name('media.upload');
                Route::post('/set-failure', [PointOfSaleDriverController::class, 'setFailure'])->name('media.upload');
            });
        });

    });

});

