<?php

use App\Http\Controllers\DashboardEmployee\CompanyProfileController;
use App\Http\Controllers\DashboardEmployee\CustomizedDriverCompanyController;
use App\Http\Controllers\DashboardEmployee\CustomizedPointCompanyController;
use App\Http\Controllers\DashboardEmployee\DefaultTripCompanyController;
use App\Http\Controllers\DashboardEmployee\DriverCompanyController;
use App\Http\Controllers\DashboardEmployee\EmployeeCompanyController;
use App\Http\Controllers\DashboardEmployee\EmployeeProfileController;
use App\Http\Controllers\DashboardEmployee\PointOfSaleCompanyController;
use App\Http\Controllers\DashboardEmployee\ProductCompanyController;
use App\Http\Controllers\DashboardEmployee\TripCompanyController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\RetailerSeller\CartRetailerSellerController;
use App\Http\Controllers\RetailerSeller\CompanyRetailerSellerController;
use App\Http\Controllers\retailerSeller\ProductRetailerSellerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//
//Route::prefix("my-account")->group(function () {
//    Route::get('/', [EmployeeProfileController::class, 'showMyAccount'])->name('media.upload');
//    Route::put('edit-profile-img', [EmployeeProfileController::class, 'editProfileImg'])->name('media.upload');
//    Route::put('edit-profile', [EmployeeProfileController::class, 'editProfile'])->name('media.upload');
//    Route::post('change-password', [EmployeeProfileController::class, 'todo'])->name('media.upload');
//});
//

Route::prefix("retailer-seller/{retailerSeller}")->group(function () {
    Route::prefix("companies")->group(function () {
        Route::get('/', [CompanyRetailerSellerController::class, 'index'])->name('media.upload');
        Route::prefix('{company}')->group(function () {
            Route::get('/products', [ProductRetailerSellerController::class, 'index'])->name('media.upload');
            Route::get('/products/{product}', [ProductRetailerSellerController::class, 'addToCart'])->name('media.upload');
        });
    });
    Route::prefix("cart")->group(function () {
        Route::get('/', [CartRetailerSellerController::class, 'index'])->name('media.upload');
        Route::post('/order', [CartRetailerSellerController::class, 'order'])->name('media.upload');
        Route::post('/{itemCart}', [CartRetailerSellerController::class, 'updateQuantity'])->name('media.upload');
    });
});
