<?php

use App\Http\Controllers\MediaController;
use App\HttpClientRequest\RequestBuilder;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/media/{media}', [MediaController::class, 'show'])->name('media.show');

Route::get('/jop/{company}', [\App\Http\Controllers\DashboardEmployee\TripCompanyController::class, 'generateTodayTrip'])->name('mediasdasdsda.show');


Route::get('/', function () {

    $df = \App\Models\DefaultTrip::query()->find(4);

    $wwww = $df->getPreview('2021-09-31');
    return $wwww;
//
//    $trip = \App\Models\Trip::query()->first();
//    $ttt = $trip->refreshPointsOfSale('33.4922374', '36.3114032');

    dd('done');

    $x['asdas'] = 'dsd';
         $x['a'] = 'test';
         $x['b'] = 'test2';
         $x['c'] = 'test3';
    dd( (object)$x);

//    $aa = 'sdasd';
//
//    $aa = \App\Models\Trip::query()->with(['defaultTrip.customizedPoints' => function($query) use($aa) {
//        $query->wheredate('date', '2021-07-07');
//    }])->first();
//    dd($aa);

    $array1 = [11, 2,3,5];
    $array2 = [11, 2,8,9];
    $result = array_diff($array1, $array2);

    dd(json_encode(array_values($result)));


//    $obj['1'] = [
//        'arrival_time' => "2020-04-08 12:30",
//        "status" => 'successful',
//        "reject_region" => 'sadasdsdasasasad',
//    ];
//
//    $obj['2'] = [
//        'arrival_time' => "2020-04-08 12:30",
//        "status" => 'successful',
//        "reject_region" => '23242432',
//    ];

//    \App\Models\Trip::query()->first()->pointsOfSale()->sync($obj, false);

//    dd('done');

//    $res = \App\Models\DefaultTrip::query()->first()->generateTrip('2020-04-04');

    dd('sadsad');

    $pointsOfSale = \App\Models\PointOfSale::query()->take(8)->get();
    dd($pointsOfSale[2]);
    $destinations = "";
    foreach ($pointsOfSale as $item){
        $destinations.=$item->lat;
        $destinations.=",";
        $destinations.=$item->long;
        $destinations.="|";
    }
    $destinations = substr($destinations, 0, -1);

    $queryData = [
        'origins' => $destinations,
        'destinations' => $destinations,
        'key'=> 'H5alPbxMYdtR3RQ2aSM4PQefc034b'
    ];

    $response = RequestBuilder::init()
        ->setQueryParam($queryData)
        ->url('https://api.distancematrix.ai/maps/api/distancematrix/json?')
        ->build()
        ->get();


    if ($response instanceof GuzzleException){
        return json_encode($response->getMessage());
    } else{
        $responseJson = $response->getBody()->getContents();
        $response = json_decode($responseJson);
        if ($response->status == 'OK'){
            $resultMatrix = $response;
        }
    }

    $result['destination_addresses'] = $resultMatrix->destination_addresses;
    $result['distances'] = [];
    $result['durations'] = [];
    foreach ($resultMatrix->rows as $row) {
        $rowDistance = [];
        $rowDuration = [];
        foreach ($row->elements as $element) {
            array_push($rowDistance, $element->distance->value);
            array_push($rowDuration, $element->duration->value);
        }
        array_push($result['distances'], $rowDistance);
        array_push($result['durations'], $rowDuration);
    }
    return json_encode($result);




//    Artisan::call('make:controller Auth/loginController');
//    Artisan::call('make:controller Auth/RegisterController');

    return view('welcome');

});
