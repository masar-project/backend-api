<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CompanyProfileController;
use App\Http\Controllers\DefaultTripCompanyController;
use App\Http\Controllers\EmployeeCompanyController;
use App\Http\Controllers\EmployeeProfileController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\PointOfSaleCompanyController;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/reset', function (){
    Artisan::call('projectCall:setup');
    return 'done';
});

Route::get('/seed', function (){
    Artisan::call('project:seed');
    return 'done';
});

Route::get('/migrate', function (){
    Artisan::call('migrate');
    return 'done';
});

Route::get('/role-update', function (){
    Artisan::call('project:role-update');
    return 'done';
});

Route::get('/work', function (){
    Artisan::call('queue:work');
    return 'done';
});

Route::get('/job', function (){
    $companies = Company::query()->get();
    foreach ($companies as $company) {
        $company->generateTodayTrip();
    }
    return 'done';
});


Route::prefix("media")->group(function () {
    Route::post('/upload', [MediaController::class, 'upload'])->name('media.upload');
});

Route::prefix("auth")->group(function () {
    Route::prefix("register")->group(function () {
        Route::post('company', [RegisterController::class, 'registerCompany'])->name('auth.registerStudent');
        Route::post('retailer', [RegisterController::class, 'registerRetailer'])->name('auth.registerStudent');
    });

    Route::prefix("login")->group(function () {
        Route::post('employee', [LoginController::class, 'loginEmployee'])->name('auth.registerStudent');
        Route::post('driver', [LoginController::class, 'loginDriver'])->name('auth.registerStudent');
        Route::post('retailer', [LoginController::class, 'loginRetailer'])->name('auth.registerStudent');
    });
});

Route::prefix('index')->group(function (){
    Route::get('cities/', [IndexController::class, 'cities'])->name('country.index');
    Route::get('categories/', [IndexController::class, 'categories'])->name('categories.index');
    Route::get('failure-reasons/', [IndexController::class, 'failureReasons'])->name('categories.index');
});






