<?php


return [
    'api_config' => [
        'per_page' => 10,
        'root_dashboard' => 'admin-panel'
    ], 'user_config' =>[
        'email_super_admin' => 'admin@admin.com',
    ], 'role_config' =>[
        'role_super_admin' => 'superAdmin',
        'role_admin' => 'admin',
        'role_manager' => 'manager'
    ], 'media_config' =>[
        'profile_img_default_male' => 1,
        'profile_img_default_female' => 2,
        'company_profile_img_default' => 3,
    ], 'disks' => [
        'file_session' => 'file_session',
    ], 'support' => [
        'folder_support_messages' => 'support_messages',
    ]
];