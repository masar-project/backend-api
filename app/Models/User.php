<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_img_id',
        'first_name',
        'last_name',
        'gender',
        'birth_date',
        'mobile_number',
        'username',
        'email',
        'password',
    ];

    public $with = ['profileImg'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /*filter*/
    public static function scopeFilter($query, $filters){

        if (isset($filters->gender) && ! empty($filters->gender)){
            $query->where('gender', $filters->gender);
        }


        if (isset($filters->key_search) && !empty($filters->key_search)){
            $s = $filters->key_search;
            $query->where(function ($q) use ($s) {
                $q->where('email', 'like', '%'.$s.'%')
                    ->orwhere('first_name', 'like', '%'.$s.'%')
                    ->orwhere('last_name', 'like', '%'.$s.'%');
            });
        }

        return $query;
    }

    public function employee(){
        return $this->hasOne(Employee::class, 'user_id');
    }

    public function retailer(){
        return $this->hasOne(RetailersSellers::class, 'user_id');
    }

    public function profileImg(){
        return $this->belongsTo(Media::class, 'profile_img_id');
    }

}
