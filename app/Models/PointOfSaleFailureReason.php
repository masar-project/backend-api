<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointOfSaleFailureReason extends Model
{
    use HasFactory;

    public $fillable = [
        'reason'
    ];

    public $primaryKey = 'id';

    public $timestamps = true;

}
