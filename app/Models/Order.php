<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = "order";

    protected $fillable = [
        'id', 'retailer_id', 'company_id', 'status',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public function retailer(){
        return $this->belongsTo(RetailersSellers::class, 'retailer_id');
    }

    public function company(){
        return $this->belongsTo(RetailersSellers::class, 'company_id');
    }

    public function itemsOrder(){
        return $this->hasMany(ItemOrder::class, 'order_id');
    }


}
