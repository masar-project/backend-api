<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemOrder extends Model
{
    use HasFactory;

    protected $table = "item_order";

    protected $fillable = [
        'id', 'order_id', 'product_id', 'quantity',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

}
