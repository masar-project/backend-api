<?php

namespace App\Models;

use App\Traits\HasTranslationsSupportUnicode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory, HasTranslationsSupportUnicode;

    protected $table = "cities";

    protected $fillable = [
        'id', 'name',
    ];

    public $translatable =[
        'name'
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public function regions(){
        return $this->hasMany(Region::class, 'city_id');
    }

    public function pointsOfSale(){
        return $this->hasMany(PointOfSale::class, 'city_id');
    }

    public function companies(){
        return $this->hasMany(Company::class, 'city_id');
    }

}
