<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomizedDriver extends Model
{
    use HasFactory;

    protected $table = "customized_drivers";

    protected $fillable = [
        'id', 'date', 'driver_id', 'default_trip_id', 'company_id'
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $with = ['driver', 'defaultTrip'];


    /*filter*/
    public static function scopeFilter($query, $filters){

        if (isset($filters['date']) && ! empty($filters['date'])){
            $query->whereDate('date',$filters['date']);
        }

        if (isset($filters['driver_id']) && ! empty($filters['driver_id'])){
            $query->where('driver_id', $filters['driver_id']);
        }

        if (isset($filters['ago_date']) && ! empty($filters['ago_date'])){
            $query->whereDate('updated_at' , '>', $filters['ago_date']);
        }

        unset($filters['ago_date']);
        $query->whereHas('driver', function ($q) use ($filters) {
            $q->filter($filters);
        });

        return $query;
    }

    public function driver(){
        return $this->belongsTo(Employee::class, 'driver_id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function defaultTrip(){
        return $this->belongsTo(DefaultTrip::class, 'default_trip_id');
    }

}
