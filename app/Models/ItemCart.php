<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemCart extends Model
{
    use HasFactory;

    protected $table = "item_cart";

    protected $fillable = [
        'id', 'retailer_id', 'product_id', 'quantity',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $with = ['retailer', 'product'];


    public function retailer(){
        return $this->belongsTo(RetailersSellers::class, 'retailer_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

}
