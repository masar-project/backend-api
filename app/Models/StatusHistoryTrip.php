<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusHistoryTrip extends Model
{
    use HasFactory;
    protected $table = "status_history_trip";

    protected $fillable = [
        'id', 'trip_id', 'status', 'action_date', 'route_distance', 'route_duration', 'lat', 'long',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public function trip(){
        return $this->belongsTo(Trip::class, 'trip_id');
    }


}
