<?php

namespace App\Models;

use App\Traits\GenerateSolutionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class DefaultTrip extends Model
{
    use HasFactory;

    protected $table = "default_trips";

    protected $fillable = [
        'id', 'day', 'name', 'company_id', 'driver_id',
    ];

    public $withCount = ['pointsOfSale'];

    public $with = ['driver'];

    protected $primaryKey = "id";

    public $timestamps = true;


    /*filter*/
    public static function scopeFilter($query, $filters){

        if (isset($filters['day']) && ! empty($filters['day'])){
            $query->where('day', $filters['day']);
        }

        if (isset($filters['driver_id']) && ! empty($filters['driver_id'])){
            $query->where('driver_id', $filters['driver_id']);
        }

        if (isset($filters['company_id']) && ! empty($filters['company_id'])){
            $query->where('company_id', $filters['company_id']);
        }

        if (isset($filters['key_search']) && !empty($filters['key_search'])){
            $s = $filters['key_search'];
            $query->where('name', 'like', '%'.$s.'%');
        }

        return $query;
    }

    public function pointsOfSale(){
        return $this->belongsToMany(PointOfSale::class, 'default_trip_point_of_sale'
            , 'default_trip_id', 'point_of_sale_id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function driver(){
        return $this->belongsTo(Employee::class, 'driver_id');
    }

    public function trips(){
        return $this->hasMany(Trip::class, 'default_trip_id');
    }

    public function customizedPoints(){
        return $this->hasMany(CustomizedPoint::class, 'default_trip_id');
    }

    public function customizedDriver(){
        return $this->hasOne(CustomizedDriver::class, 'default_trip_id');
    }


    /**
     * @param $date
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customizedPointsByDate($date){
        return $this->customizedPoints()->whereDate('date', $date);
    }

    /**
     * @param $date
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customizedDriverByDate($date){
        return $this->customizedDriver()->whereDate('date', $date);
    }






    /**
     * @param $date
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCustomizedPointsByDate($date){
        return $this->customizedPointsByDate($date)->get();
    }

    /**
     * @param $date
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCustomizedDriverByDate($date){
        $customizedDriver = $this->customizedDriverByDate($date)->firstOr(function (){
            return null;
        });

        return $customizedDriver ? $customizedDriver->driver : null;
    }

    public function getDriverAfterCustomized($date){
        return $this->getCustomizedDriverByDate($date) ?? $this->driver()->first();
    }

    public function getPointsOfSaleAfterCustomized($date){
        $defaultPointsOfSale = $this->pointsOfSale()->get();
        $customizedPoints = $this->getCustomizedPointsByDate($date);

        $deletionCustomizedPointsIds = $customizedPoints->where('type', 'deletion')
            ->pluck('point_of_sale_id');

        $additionCustomizedPoints = $customizedPoints->where('type', 'addition');


        $filteredPointsOfSale = $defaultPointsOfSale->whereNotIn('id', $deletionCustomizedPointsIds);

        foreach ($additionCustomizedPoints as $additionCustomizedPoint){
            $filteredPointsOfSale = $filteredPointsOfSale->push($additionCustomizedPoint->pointOfSale);
        }

        return $filteredPointsOfSale->unique();
    }

    public function getPreview($date){
        $result = [];
        $result['name'] = $this->name;
        $result['day'] = $this->day;
        $result['driver'] = $this->getDriverAfterCustomized($date);
        $result['pointsOfSale'] = $this->getPointsOfSaleAfterCustomized($date);

        $trip = $this->trips()->where('date', $date)->first();
        if (!$trip){
            return $result;
        }

        $pointsOfSaleTrip = $trip->pointsOfSale()->get();

        foreach ($result['pointsOfSale'] as $pointOfSale){
            foreach ($pointsOfSaleTrip as $pointOfSaleTrip){
                if ($pointOfSaleTrip->id == $pointOfSale->id){
                    $pointOfSale->default_order = $pointOfSaleTrip->pivot->default_order;
                    $pointOfSale->actual_order = $pointOfSaleTrip->pivot->actual_order;
                    $pointOfSale->arrival_time = $pointOfSaleTrip->pivot->arrival_time;
                    $pointOfSale->status = $pointOfSaleTrip->pivot->status;
                    $pointOfSale->failure_note = $pointOfSaleTrip->pivot->failure_note;
//                    dd($pointOfSale->default_order);
                }
            }
        }
//        dd($result['pointsOfSale']->get(1));

        return $result;
    }

}
