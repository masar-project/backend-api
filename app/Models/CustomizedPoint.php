<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomizedPoint extends Model
{
    use HasFactory;

    protected $table = "customized_points";

    protected $fillable = [
        'id', 'date', 'point_of_sale_id', 'default_trip_id', 'type', 'company_id'
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $with = ['pointOfSale', 'defaultTrip'];


    /*filter*/
    public static function scopeFilter($query, $filters){

        if (isset($filters['date']) && ! empty($filters['date'])){
            $query->whereDate('date',$filters['date']);
        }

        if (isset($filters['point_of_sale_id']) && ! empty($filters['point_of_sale_id'])){
            $query->where('point_of_sale_id', $filters['point_of_sale_id']);
        }

        if (isset($filters['type']) && ! empty($filters['type'])){
            $query->where('type', $filters['type']);
        }

        if (isset($filters['ago_date']) && ! empty($filters['ago_date'])){
            $query->whereDate('updated_at' , '>', $filters['ago_date']);
        }

        unset($filters['ago_date']);
        $query->whereHas('pointOfSale', function ($q) use ($filters) {
            $q->filter($filters);
        });

        return $query;
    }

    public function pointOfSale(){
        return $this->belongsTo(PointOfSale::class, 'point_of_sale_id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function defaultTrip(){
        return $this->belongsTo(DefaultTrip::class, 'default_trip_id');
    }



}
