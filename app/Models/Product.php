<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    protected $fillable = [
        'id', 'name', 'description', 'price', 'product_code', 'category_id', 'company_id', 'img_id',
    ];

    public $translatable =[
        'name',
        'description',
    ];

    protected $primaryKey = "id";

    protected $with = ['img', 'category'];

    public $timestamps = true;


    /*filter*/
    /**
     * @param $query \Illuminate\Database\Eloquent\Builder
     * @param $filters array
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopeFilter($query, $filters){

        if (isset($filters['category_id']) && ! empty($filters['category_id'])){
            $query->where('category_id', $filters['category_id']);
        }

        if (isset($filters['company_id']) && ! empty($filters['company_id'])){
            $query->where('company_id', $filters['company_id']);
        }

        if (isset($filters['key_search']) && !empty($filters['key_search'])){
            $s = $filters['key_search'];
            $query->where(function ($q) use ($s) {
                $q->where('name', 'like', '%'.$s.'%')
                    ->orwhere('description', 'like', '%'.$s.'%');
            });
        }

        return $query;
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function img(){
        return $this->belongsTo(Media::class, 'category_id');
    }

}
