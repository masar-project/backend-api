<?php

namespace App\Models;

use App\Traits\HasTranslationsSupportUnicode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, HasTranslationsSupportUnicode;

    protected $table = "categories";

    protected $fillable = [
        'id', 'parent_id', 'name',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $translatable =[
        'name'
    ];

    protected $casts = [
        'parent_id' => 'integer',
    ];

    public function subCategories(){
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent(){
        return $this->belongsTo(Category::class, 'parent_id');
    }

}
