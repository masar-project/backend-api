<?php

namespace App\Models;

use App\Traits\HasTranslationsSupportUnicode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RetailersSellers extends Model
{
    use HasFactory, HasTranslationsSupportUnicode;

    protected $table = "retailers_sellers";

    protected $fillable = [
        'id', 'name', 'user_id', 'city_id', 'region_id', 'phone_number', 'work_times', 'lat', 'long', 'address',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $translatable =[
        'name'
    ];

    public $with = ['user', 'city', 'region'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function region(){
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function itemsCart(){
        return $this->hasMany(ItemCart::class, 'retailer_id');
    }

    public function order(){
        return $this->hasMany(Order::class, 'retailer_id');
    }

}
