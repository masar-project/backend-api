<?php

namespace App\Models;

use App\Traits\HasTranslationsSupportUnicode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory, HasTranslationsSupportUnicode;

    protected $table = "regions";

    protected $fillable = [
        'id', 'name', 'city_id',
    ];

    public $translatable =[
        'name'
    ];


    protected $primaryKey = "id";

    public $timestamps = true;

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function companies(){
        return $this->hasMany(Company::class, 'region_id');
    }

    public function pointsOfSale(){
        return $this->hasMany(PointOfSale::class, 'region_id');
    }


}
