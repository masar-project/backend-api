<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $table = "media";

    protected $fillable = [
        'id', 'type', 'used', 'path', 'disk', 'owner_id',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public function owner(){
        return $this->belongsTo('App\Models\User', 'owner_id');
    }
}
