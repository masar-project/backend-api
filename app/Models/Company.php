<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = "companies";


    protected $fillable = [
        'id', 'name', 'img_id', 'city_id', 'region_id', 'address', 'lat', 'long',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $with = ['img', 'city', 'region'];


    /*filter*/
    /**
     * @param $query \Illuminate\Database\Eloquent\Builder
     * @param $filters array
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopeFilter($query, $filters){

        if (isset($filters['city_id']) && ! empty($filters['city_id'])){
            $query->where('city_id', $filters['city_id']);
        }

        if (isset($filters['region_id']) && ! empty($filters['region_id'])){
            $query->where('region_id', $filters['region_id']);
        }

        if (isset($filters['category_id']) && !empty($filters['category_id'])){
            $query->whereHas('categories', function ($q) use ($filters) {
//            $filters->query = new ParameterBag($filters);
                $q->where('category_id', $filters['category_id']);
            });
        }

        if (isset($filters['key_search']) && !empty($filters['key_search'])){
            $s = $filters['key_search'];
            $query->where(function ($q) use ($s) {
                $q->where('name', 'like', '%'.$s.'%')
                    ->orwhere('address', 'like', '%'.$s.'%');
            });
        }

        return $query;
    }

    public function categories(){
        return $this->belongsToMany(Category::class, 'company_category', 'company_id');
    }

    public function pointsOfSale(){
        return $this->hasMany(PointOfSale::class, 'company_id');
    }

    public function employees(){
        return $this->hasMany(Employee::class, 'company_id')->employees();
    }

    public function drivers(){
        return $this->hasMany(Employee::class, 'company_id')->drivers();
    }

    public function products(){
        return $this->hasMany(Product::class, 'company_id');
    }

    public function trips(){
        return $this->hasMany(Trip::class, 'company_id');
    }

    public function defaultTrips(){
        return $this->hasMany(DefaultTrip::class, 'company_id');
    }

    public function customizedPoints(){
        return $this->hasMany(CustomizedPoint::class, 'company_id');
    }

    public function customizedDrivers(){
        return $this->hasMany(CustomizedDriver::class, 'company_id');
    }

    public function img(){
        return $this->belongsTo(Media::class, 'img_id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function region(){
        return $this->belongsTo(Region::class, 'region_id');
    }


    /**
     * @param $date
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDefaultTripInDate($date){
        return $this->defaultTrips()->where('day', Carbon::make($date)->format('l'))->get();
    }

    public function generateTodayTrip(){
        $date = now();

        $defaultTrips = $this->getDefaultTripInDate($date);
        foreach ($defaultTrips as $defaultTrip){
            $previewTrip = $defaultTrip->getPreview($date);

            $trip = $defaultTrip->trips()->updateOrCreate(
                [
                    'date' => $date->format('Y-m-d')
                ],
                [
                    'driver_id' => $previewTrip['driver']['id'],
                    'name' => $defaultTrip->name,
                    'company_id' => $this->id,
                ]
            );

            $trip->refreshDriver();
            $trip->refreshPointsOfSale($this->lat, $this->long);
        }
    }
}
