<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class PointOfSale extends Model
{
    use HasFactory;

    protected $table = "points_of_sale";

    protected $fillable = [
        'id', 'name', 'work_times', 'note', 'company_id', 'city_id', 'region_id', 'address', 'lat', 'long', 'disable', 'action_disable_employee_id',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $with = ['city', 'region'];


    /*filter*/
    public static function scopeFilter($query, $filters){

        if (isset($filters['disable']) && ! empty($filters['disable'])){
            $query->where('disable', $filters->disable);
        }

        if (isset($filters->city_id) && ! empty($filters->city_id)){
            $query->where('city_id', $filters->city_id);
        }

        if (isset($filters->region_id) && ! empty($filters->region_id)){
            $query->where('region_id', $filters->region_id);
        }

        if (isset($filters->key_search) && !empty($filters->key_search)){
            $s = $filters->key_search;
            $query->where(function ($q) use ($s) {
                $q->where('name', 'like', '%'.$s.'%')
                    ->orwhere('note', 'like', '%'.$s.'%')
                    ->orwhere('address', 'like', '%'.$s.'%');
            });
        }

        return $query;
    }





    public function company(){

        return $this->belongsTo(Company::class, 'company_id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }

    public function region(){
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function defaultTrips(){
        return $this->belongsToMany(DefaultTrip::class, 'default_trip_point_of_sale', 'point_of_sale_id');
    }

    public function trips(){
        return $this->belongsToMany(Trip::class, 'point_of_sale_trip', 'point_of_sale_id');
    }

    public function customizedPoints(){
        return $this->hasMany(CustomizedPoint::class, 'point_of_sale_id');
    }

}
