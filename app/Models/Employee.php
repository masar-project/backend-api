<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

class Employee extends Model
{
    use HasFactory;

    protected $table = "employees";

    protected $fillable = [
        'id', 'user_id', 'company_id', 'role', 'disable', 'action_disable_employee_id',
    ];

    public $with = ['user'];

    protected $primaryKey = "id";

    public $timestamps = true;


    /*filter*/
    /**
     * @param $query \Illuminate\Database\Eloquent\Builder
     * @param $filters array
     * @return
     */
    public static function scopeFilter($query, $filters){

        if (isset($filters['disable']) && ! empty($filters['disable'])){
            $query->where('disable', $filters['disable']);
        }

        if (isset($filters['role']) && ! empty($filters['role'])){
            $query->where('role', $filters['role']);
        }

        unset($filters['ago_date']);
        $query->whereHas('user', function ($q) use ($filters) {
//            $filters->query = new ParameterBag($filters);
            $q->filter($filters);
        });

        return $query;
    }

    protected function scopeEmployees($query){
        return $query->where('role', 'manager')->orWhere('role', 'normal');
    }

    protected function scopeDrivers($query){
        return $query->where('role', 'driver');
    }

    /*query*/

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function employees(){
        return self::query()->employees();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function drivers(){
        return self::query()->drivers();
    }



    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function defaultTrips(){
        return $this->hasMany(DefaultTrip::class, 'driver_id');
    }

    public function trips(){
        return $this->hasMany(Trip::class, 'driver_id');
    }

}
