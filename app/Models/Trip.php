<?php

namespace App\Models;

use App\Traits\GenerateSolutionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;
    use GenerateSolutionTrait;

    protected $table = "trips";

    protected $fillable = [
        'id', 'date', 'name', 'company_id', 'driver_id', 'default_trip_id', 'status', 'route_distance', 'route_duration'
        , 'start_at', 'done_at',
    ];

    protected $primaryKey = "id";

    public $timestamps = true;

    public $withCount = ['pointsOfSale'];

    public $with = ['statusHistoryTrip'];


    /*filter*/
    public static function scopeFilter($query, $filters){

        if (isset($filters['date']) && ! empty($filters['date'])){
            $query->whereDate('date', $filters['date']);
        }

        if (isset($filters['driver_id']) && ! empty($filters['driver_id'])){
            $query->where('driver_id', $filters['driver_id']);
        }

        if (isset($filters['company_id']) && ! empty($filters['company_id'])){
            $query->where('company_id', $filters['company_id']);
        }

        if (isset($filters['default_trip_id']) && ! empty($filters['default_trip_id'])){
            $query->where('default_trip_id', $filters['default_trip_id']);
        }

        if (isset($filters['status']) && ! empty($filters['status'])){
            $query->where('status', $filters['status']);
        }

        return $query;
    }


    public function statusHistoryTrip(){
        return $this->hasMany(StatusHistoryTrip::class, 'trip_id');
    }

    public function pointsOfSale(){
        return $this->belongsToMany(PointOfSale::class, 'point_of_sale_trip', 'trip_id')
            ->withPivot('failure_note', 'status', 'arrival_time', 'default_order', 'actual_order', 'point_of_sale_failure_reason_id')->orderBy('default_order');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function driver(){
        return $this->belongsTo(Employee::class, 'driver_id');
    }

    public function defaultTrip(){
        return $this->belongsTo(DefaultTrip::class, 'default_trip_id');
    }

    public function pointsOfSaleWhereStatus($statuses){
        $statuses = is_array($statuses) ? $statuses : array($statuses);
        return $this->pointsOfSale()->whereIn('point_of_sale_trip.status', $statuses);
    }

    public function pointsOfSaleWhereNotStatus($statuses){
        $statuses = is_array($statuses) ? $statuses : array($statuses);
        return $this->pointsOfSale()->whereNotIn('point_of_sale_trip.status', $statuses);
    }

    public function vestedPointOfSale(){
        return $this->pointsOfSaleWhereStatus(['success', 'failure']);
    }

    public function notVestedPointOfSale(){
        return $this->pointsOfSaleWhereNotStatus(['success', 'failure']);
    }


    public function getPointsOfSaleWhereStatus($statuses){
        return $this->pointsOfSaleWhereStatus($statuses)->get();
    }

    public function getPointsOfSaleWhereNotStatus($statuses){
        return $this->pointsOfSaleWhereNotStatus($statuses)->get();
    }

    public function getVestedPoint(){
        return $this->vestedPointOfSale()->get();
    }

    public function getNotVestedPoint(){
        return $this->notVestedPointOfSale()->get();
    }

    public function getCountVestedPoint(){
        return $this->vestedPointOfSale()->count();
    }

    public function getCountNotVestedPoint(){
        return $this->notVestedPointOfSale()->count();
    }

    public function setPointOfSaleStatusIntransit($pointOfSaleId){
        $obj[$pointOfSaleId] = [
            'status' => 'intransit'
        ];
        $this->pointsOfSale()->syncWithoutDetaching($obj);
        return true;
    }

    public function setPointOfSaleStatusSuccess($pointOfSaleId){
        $obj[$pointOfSaleId] = [
            'status' => 'success',
            'actual_order' => $this->getCountVestedPoint()+1,
            'arrival_time' => now()->format('Y-m-d H:i')
        ];
        $this->pointsOfSale()->syncWithoutDetaching($obj);
        $this->checkFinishAllPoint();
        return true;
    }

    public function setPointOfSaleStatusFailure($pointOfSaleId, $failureNote, $pointOfSaleFailureReasonId){

        $obj[$pointOfSaleId] = [
            'status' => 'failure',
            'actual_order' => $this->getCountVestedPoint()+1,
            'arrival_time' => now()->format('Y-m-d H:i'),
            'failure_note' => $failureNote,
            'point_of_sale_failure_reason_id' => $pointOfSaleFailureReasonId
        ];

        $this->pointsOfSale()->syncWithoutDetaching($obj);
        $this->checkFinishAllPoint();
        return true;
    }

    public function checkFinishAllPoint(){
        if (!$this->pointsOfSale()->whereIn('status', ['idle', 'intransit'])->exists()){
            $this->update([
                'status' => 'done'
            ]);
        }
    }

    public function refreshDriver(){
        $driver = $this->defaultTrip->getDriverAfterCustomized($this->date);

        $this->update([
            'driver_id' => $driver->id
        ]);
    }

    public function refreshPointsOfSale($currentLat, $currentLong)
    {
        $pointsOfSaleAfterCustomized = $this->defaultTrip->getPointsOfSaleAfterCustomized($this->date);


        //remove deletion points if exists
        $pointsOfSaleIds = $pointsOfSaleAfterCustomized->pluck('id')->toArray();
        $currentIdlePointOfSaleId = $this->getPointsOfSaleWhereStatus('idle')
            ->pluck('id')->toArray();
        $pointsIdsRemoved = array_values(array_diff($currentIdlePointOfSaleId, $pointsOfSaleIds));
        $this->pointsOfSale()->detach($pointsIdsRemoved);


        $this->refreshSolution($currentLat, $currentLong, $pointsOfSaleAfterCustomized);
    }

    public function refreshSolution($currentLat, $currentLong, $pointsOfSaleAfterCustomized = null){

        $vestedPointOfSaleIds = $this->getVestedPoint()->pluck('id')->toArray();
        if (!$pointsOfSaleAfterCustomized)
            $pointsOfSaleAfterCustomizedNotVested = $this->getNotVestedPoint();
        else{
            $pointsOfSaleAfterCustomizedNotVested = $pointsOfSaleAfterCustomized->whereNotIn('id', $vestedPointOfSaleIds);
        }

        $countVested = count($vestedPointOfSaleIds);

        $resultSolution = $this->generateSolution($currentLat, $currentLong, $pointsOfSaleAfterCustomizedNotVested);

        if ($this->status == 'active'){
            if ($this->statusHistoryTrip()->count() == 0){
                $this->update([
                    'start_at' => now(),
                    'route_distance' => $resultSolution['route_distance'],
                    'route_duration' => $resultSolution['route_duration']
                ]);
            }

            $this->statusHistoryTrip()->create([
                'status' => 'start',
                'action_date' => now(),
                'route_distance' => $resultSolution['route_distance'],
                'route_duration' => $resultSolution['route_duration'],
                'lat' => $currentLat,
                'long' => $currentLong,
            ]);
        }

        foreach ($resultSolution['sortedResult'] as $i => $pointOfSale){
            $pointsOfSaleStatusTrip[$pointOfSale['id']] = [
                'created_at' => now(),
                'updated_at' => now(),
                'default_order' => $countVested+$i+1
            ];
        }

        foreach ($resultSolution['dropped_nodes_points'] as $i => $pointOfSale){
            $pointsOfSaleStatusTrip[$pointOfSale['id']] = [
                'created_at' => now(),
                'updated_at' => now(),
                'status' => 'dropped',
                'default_order' => 9999
            ];
        }

        if (isset($pointsOfSaleStatusTrip)) {
            $this->pointsOfSale()->syncWithoutDetaching($pointsOfSaleStatusTrip);
        }else{
            $this->pointsOfSale()->detach();
        }
        $this->checkFinishAllPoint();
    }

}
