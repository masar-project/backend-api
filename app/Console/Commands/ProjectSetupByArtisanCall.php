<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ProjectSetupByArtisanCall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projectCall:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'setup project add seed and more By Artisan Call';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Schema::disableForeignKeyConstraints();
        Artisan::call('migrate:fresh');
        Schema::enableForeignKeyConstraints();
        Artisan::call('migrate', ['--path' => 'vendor/laravel/passport/database/migrations']);
        Artisan::call('db:seed');
        Artisan::call('passport:install');
        Artisan::call('trip:create');
        return 0;
    }
}
