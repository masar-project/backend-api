<?php

namespace App\Http\Resources\Auth\register;

use App\Http\Resources\DashboardEmployee\Employee\EmployeeCompanyResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;


class RegisterCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "company" => CompanyResource::make($this['employee']),
            "employee" => EmployeeCompanyResource::make($this['employee']),
            'tokenType' => $this['token_type'],
            'accessToken' => $this['access_token'],
        ];
    }
}
