<?php

namespace App\Http\Resources\Auth\register;

use App\Http\Resources\DashboardEmployee\Employee\EmployeeCompanyResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\Retailer\RetailerResource;
use Illuminate\Http\Resources\Json\JsonResource;


class RegisterRetailerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "retailer" => RetailerResource::make($this['retailer']),
            'tokenType' => $this['token_type'],
            'accessToken' => $this['access_token'],
        ];
    }
}
