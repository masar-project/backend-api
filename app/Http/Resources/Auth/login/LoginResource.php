<?php

namespace App\Http\Resources\Auth\login;

use App\Http\Resources\DashboardEmployee\Employee\EmployeeCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "employee" => EmployeeCompanyResource::make($this['employee']),
            'tokenType' => $this['token_type'],
            'accessToken' => $this['access_token'],
        ];
    }
}
