<?php

namespace App\Http\Resources\DashboardDriver\Trip;

use App\Http\Resources\DashboardDriver\DefaultTrip\DefaultTripDriverResource;
use App\Http\Resources\DashboardDriver\PointOfSale\PointOfSaleDriverResource;
use App\Http\Resources\DashboardEmployee\Driver\DriverCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StatusHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "status" => $this->date,
            "actionDate" => $this->action_date,
            "routeDistance" => $this->route_distance,
            "routeDuration" => $this->route_duration,
            "lat" => $this->lat,
            "long" => $this->long,
        ];
    }

}
