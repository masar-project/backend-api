<?php

namespace App\Http\Resources\DashboardDriver\DefaultTrip;

use App\Http\Resources\DashboardEmployee\Company\PointsOfSale\PointOfSaleCompanyResource;
use App\Http\Resources\DashboardEmployee\Employee\EmployeeCompanyResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DefaultTripDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "day" => $this->day,
            "pointsOfSaleCount" => $this->points_of_sale_count,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "pointsOfSale" => PointOfSaleCompanyResource::collection($this->whenLoaded('pointsOfSale')),
            "driver" => EmployeeCompanyResource::make($this->whenLoaded('driver')),
            "company" => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
