<?php

namespace App\Http\Resources\DashboardDriver\DefaultTrip;

use App\Http\Resources\DashboardDriver\Trip\TripDriverResource;
use Illuminate\Http\Resources\Json\JsonResource;

class GenerateTripFromDefaultTripDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'trip' => TripDriverResource::make($this['trip']),
            'routeDistance' => $this['route_distance'],
            'routeDuration' => $this['route_duration'],
        ];
    }

}
