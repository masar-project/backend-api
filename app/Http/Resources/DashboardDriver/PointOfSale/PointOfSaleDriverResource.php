<?php


namespace App\Http\Resources\DashboardDriver\PointOfSale;

use App\Http\Resources\DashboardEmployee\Company\PointsOfSale\WorkTimesResource;
use App\Http\Resources\Global_\City\CityResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PointOfSaleDriverResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "note" => $this->note,
            "address" => $this->address,
            "lat" => $this->lat,
            "long" => $this->long,

            'arrivalTime' => $this->whenPivotLoaded('point_of_sale_trip', function (){
                return $this->pivot->arrival_time;
            }),
            'actualOrder' => $this->whenPivotLoaded('point_of_sale_trip', function (){
                return $this->pivot->actual_order;
            }),
            'defaultOrder' => $this->whenPivotLoaded('point_of_sale_trip', function (){
                return $this->pivot->default_order;
            }),
            'status' => $this->whenPivotLoaded('point_of_sale_trip', function (){
                return $this->pivot->status;
            }),
            'pointOfSaleFailureReason' => $this->whenPivotLoaded('point_of_sale_trip', function (){
                return $this->pivot->point_of_sale_failure_reason_id;
            }),
            'failureNote' => $this->whenPivotLoaded('point_of_sale_trip', function (){
                return $this->pivot->failure_note;
            }),

            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "workTimes" => WorkTimesResource::make(json_decode($this->work_times)),
            "city" => CityResource::make($this->whenLoaded('city')),
            "region" => RegionResource::make($this->whenLoaded('region')),
            "company" => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
