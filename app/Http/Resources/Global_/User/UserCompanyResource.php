<?php

namespace App\Http\Resources\Global_\User;

use App\Http\Resources\Global_\Media\MediaResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "firstName" => $this->first_name,
            "lastName" => $this->last_name,
            "gender" => $this->gender,
            "birthDate" => $this->birth_date,
            "mobileNumber" => $this->mobile_number,
            "email" => $this->email,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "profileImg" => MediaResource::make($this->whenLoaded('profileImg')),
        ];
    }
}
