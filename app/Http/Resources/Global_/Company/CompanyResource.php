<?php

namespace App\Http\Resources\Global_\Company;

use App\Http\Resources\Global_\City\CityResource;
use App\Http\Resources\Global_\Media\MediaResource;
use App\Http\Resources\Global_\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "address" => $this->address,
            "lat" => $this->lat,
            "long" => $this->long,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "img" => MediaResource::make($this->whenLoaded('img')),
            "city" => CityResource::make($this->whenLoaded('city')),
            "region" => RegionResource::make($this->whenLoaded('region')),
        ];
    }
}
