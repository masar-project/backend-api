<?php

namespace App\Http\Resources\Global_\Retailer;

use App\Http\Resources\Global_\Media\MediaResource;
use App\Http\Resources\Global_\User\UserCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RetailerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "profile_code" => $this->profile_code,
            "phone_number" => $this->phone_number,
            "work_times" => $this->birth_date,
            "lat" => $this->lat,
            "long" => $this->long,
            "address" => $this->address,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'user' => UserCompanyResource::make($this->whenLoaded('user')),
            'city' => UserCompanyResource::make($this->whenLoaded('city')),
            'region' => UserCompanyResource::make($this->whenLoaded('region')),
        ];
    }
}
