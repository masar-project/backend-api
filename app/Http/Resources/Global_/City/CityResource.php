<?php

namespace App\Http\Resources\Global_\City;

use App\Http\Resources\Global_\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "nameTranslations" => $this->getTranslations('name'),
            "regions" => RegionResource::collection($this->whenLoaded('regions'))
        ];
    }
}
