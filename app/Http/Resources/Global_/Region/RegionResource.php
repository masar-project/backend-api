<?php

namespace App\Http\Resources\Global_\Region;

use App\Http\Resources\Global_\City\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RegionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "cityId" => $this->city_id,
            "nameTranslations" => $this->getTranslations('name'),
            "city" => CityResource::make($this->whenLoaded('city'))
        ];
    }
}
