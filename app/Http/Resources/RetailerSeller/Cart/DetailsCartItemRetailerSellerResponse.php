<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 30/8/2021
 * Time: 10:05 PM
 */

namespace App\Http\Resources\RetailerSeller\Cart;


use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\Retailer\RetailerResource;
use App\Http\Resources\RetailerSeller\Product\ProductRetailerSellerResource;
use App\Models\RetailersSellers;
use Illuminate\Http\Resources\Json\JsonResource;

class DetailsCartItemRetailerSellerResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" =>$this->id,
            "quantity" =>$this->quantity,
            "retailer" => RetailerResource::make($this->whenLoaded('retailer')),
            "product" => ProductRetailerSellerResource::make($this->whenLoaded('product')),
        ];
    }
}
