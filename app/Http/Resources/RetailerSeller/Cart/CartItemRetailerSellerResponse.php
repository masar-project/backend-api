<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 30/8/2021
 * Time: 10:05 PM
 */

namespace App\Http\Resources\RetailerSeller\Cart;


use App\Http\Resources\Global_\Company\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItemRetailerSellerResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "company" => CompanyResource::make($this['company']),
            "itemsCompanyCart" => DetailsCartItemRetailerSellerResponse::collection($this['itemsCompanyCart']),
        ];
    }
}
