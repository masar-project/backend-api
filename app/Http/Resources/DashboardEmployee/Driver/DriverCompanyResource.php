<?php

namespace App\Http\Resources\DashboardEmployee\Driver;

use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\User\UserCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'role' => $this->role,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'user' => UserCompanyResource::make($this->whenLoaded('user')),
            'company' => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
