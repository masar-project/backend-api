<?php

namespace App\Http\Resources\DashboardEmployee\Employee;

use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\User\UserCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'role' => $this->role,
            "companyId" => $this->company_id,
            "disable" => $this->disable,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'user' => UserCompanyResource::make($this->whenLoaded('user')),
            'company' => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
