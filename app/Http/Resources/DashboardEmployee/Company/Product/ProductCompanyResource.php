<?php

namespace App\Http\Resources\DashboardEmployee\Company\Product;

use App\Http\Resources\Global_\Category\CategoryResource;
use App\Http\Resources\Global_\City\CityResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\Media\MediaResource;
use App\Http\Resources\Global_\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->note,
            "price" => $this->address,
            "product_code" => $this->lat,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "img" => MediaResource::make($this->whenLoaded('img')),
            "company" => CompanyResource::make($this->whenLoaded('company')),
            "category" => CategoryResource::make($this->whenLoaded('category')),
        ];
    }
}
