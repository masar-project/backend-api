<?php

namespace App\Http\Resources\DashboardEmployee\Company\CustomizedDriver;

use App\Http\Resources\DashboardEmployee\Company\DefaultTrip\DefaultTripCompanyResource;
use App\Http\Resources\DashboardEmployee\Driver\DriverCompanyResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomizedDriverCompanyResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "date" => $this->date,
            "companyId" => $this->company_id,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "defaultTrip" => DefaultTripCompanyResource::make($this->whenLoaded('defaultTrip')),
            "driver" => DriverCompanyResource::make($this->whenLoaded('driver')),
            "company" => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
