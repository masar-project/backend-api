<?php

namespace App\Http\Resources\DashboardEmployee\Company\PointsOfSale;

use App\Http\Resources\Global_\City\CityResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use App\Http\Resources\Global_\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PointOfSaleCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "note" => $this->note,
            "address" => $this->address,
            "lat" => $this->lat,
            "long" => $this->long,
            "disable" => $this->disable,
            "defaultOrder" => $this->default_order,
            "status" => $this->status,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "workTimes" => WorkTimesResource::make(json_decode($this->work_times)),
            "city" => CityResource::make($this->whenLoaded('city')),
            "region" => RegionResource::make($this->whenLoaded('region')),
            "company" => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
