<?php

namespace App\Http\Resources\DashboardEmployee\Company\PointsOfSale;

use Illuminate\Http\Resources\Json\JsonResource;

class WorkTimeItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'open' => $this->open,
            'close' => $this->close,
            'vacation' => $this->vacation,
        ];
    }
}
