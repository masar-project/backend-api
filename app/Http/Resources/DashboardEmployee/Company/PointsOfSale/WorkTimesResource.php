<?php

namespace App\Http\Resources\DashboardEmployee\Company\PointsOfSale;

use Illuminate\Http\Resources\Json\JsonResource;

class WorkTimesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Saturday' => WorkTimeItemResource::make($this->Saturday),
            'Sunday' => WorkTimeItemResource::make($this->Sunday),
            'Monday' => WorkTimeItemResource::make($this->Monday),
            'Tuesday' => WorkTimeItemResource::make($this->Tuesday),
            'Wednesday' => WorkTimeItemResource::make($this->Wednesday),
            'Thursday' => WorkTimeItemResource::make($this->Thursday),
            'Friday' => WorkTimeItemResource::make($this->Friday),
        ];
    }
}
