<?php

namespace App\Http\Resources\DashboardEmployee\Company\CustomizedPoint;

use App\Http\Resources\DashboardEmployee\Company\DefaultTrip\DefaultTripCompanyResource;
use App\Http\Resources\DashboardEmployee\Company\PointsOfSale\PointOfSaleCompanyResource;
use App\Http\Resources\Global_\Company\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomizedPointCompanyResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "date" => $this->date,
            "type" => $this->type,
            "companyId" => $this->company_id,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            "defaultTrip" => DefaultTripCompanyResource::make($this->whenLoaded('defaultTrip')),
            "pointsOfSale" => PointOfSaleCompanyResource::make($this->whenLoaded('pointOfSale')),
            "company" => CompanyResource::make($this->whenLoaded('company')),
        ];
    }
}
