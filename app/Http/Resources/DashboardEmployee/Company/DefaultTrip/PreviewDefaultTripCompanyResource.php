<?php

namespace App\Http\Resources\DashboardEmployee\Company\DefaultTrip;

use App\Http\Resources\DashboardEmployee\Company\PointsOfSale\PointOfSaleCompanyResource;
use App\Http\Resources\DashboardEmployee\Driver\DriverCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PreviewDefaultTripCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "name" => $this['name'],
            "day" => $this['day'],
            "driver" => DriverCompanyResource::make($this['driver']),
            "pointsOfSale" => PointOfSaleCompanyResource::collection($this['pointsOfSale']),
        ];
    }
}
