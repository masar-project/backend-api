<?php

namespace App\Http\Resources\DashboardEmployee\Company\Trip;

use App\Http\Resources\DashboardDriver\DefaultTrip\DefaultTripDriverResource;
use App\Http\Resources\DashboardDriver\PointOfSale\PointOfSaleDriverResource;
use App\Http\Resources\DashboardEmployee\Driver\DriverCompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TripCompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "date" => $this->date,
            "name" => $this->name,
            "status" => $this->status,
            "startAt" => $this->start_at,
            "doneAt" => $this->done_at,
            "defaultTripId" => $this->default_trip_id,
            "pointsOfSaleCount" => $this->points_of_sale_count,
            "routeDistance" => $this->route_distance,
            "routeDuration" => $this->route_duration,

            "driver" => DriverCompanyResource::make($this->whenLoaded('driver')),//todo
            "defaultTrip" => DefaultTripDriverResource::make($this->whenLoaded('defaultTrip')),
            "pointsOfSaleSorted" => PointOfSaleDriverResource::collection($this->whenLoaded('pointsOfSale')),

//            "droppedNodes" => PointOfSaleCompanyResource::collection($this),
        ];
    }

}
