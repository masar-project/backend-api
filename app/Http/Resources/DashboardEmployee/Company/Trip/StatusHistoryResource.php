<?php

namespace App\Http\Resources\DashboardEmployee\Company\Trip;

use Illuminate\Http\Resources\Json\JsonResource;

class StatusHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "status" => $this->date,
            "actionDate" => $this->action_date,
            "routeDistance" => $this->route_distance,
            "routeDuration" => $this->route_duration,
            "lat" => $this->lat,
            "long" => $this->long,
        ];
    }

}
