<?php

namespace App\Http\Controllers;

use App\Http\Resources\Global_\Category\CategoryResource;
use App\Http\Resources\Global_\City\CityResource;
use App\Http\Resources\Global_\PointOfSaleFailureReason\PointOfSaleFailureReasonResource;
use App\Models\Category;
use App\Models\City;
use App\Models\PointOfSaleFailureReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class IndexController extends Controller
{

    public function cities(Request $request){

        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $cities = City::query()->with('regions')
            ->paginate($per_page);

        return $this->sendResponseResource(CityResource::collection($cities));
    }

    public function categories(Request $request){
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $categories = Category::query()
            ->whereNull('parent_id')
            ->with(['subCategories'])
            ->paginate($per_page);

        return $this->sendResponseResource(CategoryResource::collection($categories));
    }

    public function failureReasons(Request $request){
        $pointOfSaleFailureReasons = PointOfSaleFailureReason::query()->get();
        return $this->sendResponseResource(PointOfSaleFailureReasonResource::collection($pointOfSaleFailureReasons));
    }
}
