<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Login\LoginDriverRequest;
use App\Http\Requests\Auth\Login\LoginEmployeeRequest;
use App\Http\Requests\Auth\Login\LoginRetailerRequest;
use App\Http\Resources\Auth\login\LoginResource;
use App\Http\Resources\Auth\login\LoginRetailerResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function loginEmployee(LoginEmployeeRequest $request){

        $credentials = $request->validated();

        if(!Auth::attempt($credentials)){
            return $this->sendMessageError("Unauthorized", Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        if (!$user->employee()->exists() || $user->employee->role == "driver"){
            return $this->sendMessageError("Unauthorized just mobile app you are driver", Response::HTTP_UNAUTHORIZED);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = now()->addWeeks(1);
        $token->save();

        $data["employee"] = $user->employee;
        $data["access_token"] = $tokenResult->accessToken;
        $data["token_type"] = 'Bearer';

        return $this->sendResponseResource(LoginResource::make($data));
    }

    public function loginDriver(LoginDriverRequest $request){

        $validatedData = $request->validated();

//        $credentials = [
//            'username' => $validatedData('username'),
//            'password' => $validatedData('password'),
//        ];

        if(!Auth::attempt($validatedData)){
            return $this->sendMessageError("Unauthorized", Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();

        if (!$user->employee()->exists() || $user->employee->role !== "driver"){
            return $this->sendMessageError("Unauthorized just web app you are not driver", Response::HTTP_UNAUTHORIZED);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = now()->addWeeks(1);
        $token->save();

        $data["employee"] = $user->employee;
        $data["access_token"] = $tokenResult->accessToken;
        $data["token_type"] = 'Bearer';
        return $this->sendResponseResource(LoginResource::make($data));
    }

    public function loginRetailer(LoginRetailerRequest $request){

        $validatedData = $request->validated();

//        $credentials = [
//            'username' => $validatedData('username'),
//            'password' => $validatedData('password'),
//        ];

        if(!Auth::attempt($validatedData)){
            return $this->sendMessageError("Unauthorized", Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        if (!$user->retailer()->exists()){
            return $this->sendMessageError("Unauthorized you are not retailer", Response::HTTP_UNAUTHORIZED);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = now()->addWeeks(1);
        $token->save();

        $data["retailer"] = $user->retailer;
        $data["access_token"] = $tokenResult->accessToken;
        $data["token_type"] = 'Bearer';
        return $this->sendResponseResource(LoginRetailerResource::make($data));
    }


}
