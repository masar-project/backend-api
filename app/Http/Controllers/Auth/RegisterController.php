<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Register\RegisterCompanyRequest;
use App\Http\Requests\Auth\Register\RegisterRetailerRequest;
use App\Http\Resources\Auth\register\RegisterCompanyResource;
use App\Http\Resources\Auth\register\RegisterRetailerResource;
use App\Models\Company;
use App\Models\RetailersSellers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    /**
     * @param RegisterCompanyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerCompany(RegisterCompanyRequest $request){

        $validatedData  = $request->validated();

        $user = User::query()->create([
            'profile_img_id' =>  Config::get('constants.media_config.profile_img_default_'.$validatedData['gender']),
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'gender' => $validatedData['gender'],
            'mobile_number' => $validatedData['mobile_number'],
            'birth_date' => $validatedData['birth_date'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
        ]);

        $company = Company::query()->create([
            'name' => $validatedData['company']['name'],
            'img_id' => Config::get('constants.media_config.company_profile_img_default'),
            'city_id' => $validatedData['company']['city_id'],
            'region_id' => $validatedData['company']['region_id'],
            'address' => $validatedData['company']['address'],
            'lat' => $validatedData['company']['lat'],
            'long' => $validatedData['company']['long'],
        ]);

        $user->employee()->create([
            'company_id' => $company->id,
            'role' => 'owner',
        ]);

        //add token to user
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = now()->addMonths(1);
        $token->save();

        $data["company"] = $company->fresh();
        $data["employee"] = $user->employee;
        $data["access_token"] = $tokenResult->accessToken;
        $data["token_type"] = 'Bearer';
        return $this->sendResponseResource(RegisterCompanyResource::make($data));
    }


    /**
     * @param RegisterRetailerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerRetailer(RegisterRetailerRequest $request){

        $validatedData = $request->validated();

        $user = User::query()->create([
            'profile_img_id' =>  Config::get('constants.media_config.profile_img_default_'.$validatedData['gender']),
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'gender' => $validatedData['gender'],
            'mobile_number' => $validatedData['mobile_number'],
            'birth_date' => $validatedData['birth_date'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
        ]);

        $retailer = $user->retailer()->create([
            'name' => $validatedData['retailer']['name'],
            'city_id' => $validatedData['retailer']['city_id'],
            'region_id' => $validatedData['retailer']['region_id'],
            'address' => $validatedData['retailer']['address'],
            'lat' => $validatedData['retailer']['lat'],
            'long' => $validatedData['retailer']['long'],
            'phone_number' => $validatedData['retailer']['phone_number'],
        ]);

        //add token to user
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = now()->addMonths(1);
        $token->save();

        $data["retailer"] = $retailer->fresh();
        $data["access_token"] = $tokenResult->accessToken;
        $data["token_type"] = 'Bearer';
        return $this->sendResponseResource(RegisterRetailerResource::make($data));
    }


}
