<?php

namespace App\Http\Controllers;

use App\helper\MediaType;
use App\Http\Resources\Media\MediaResource;
use App\Models\Media;
use App\Traits\FileUploadSearchTutorTrait;
use App\Traits\FileManagementTrait;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MediaController extends Controller
{

    use FileUploadSearchTutorTrait;


    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only('upload');
    }


    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function upload(Request $request)
    {
        Validator::make($request->all(),[
            'used' => ['required', 'string', Rule::in(MediaType::listUsed())],
            'type' => ['required', 'string', Rule::in(MediaType::listTypesFromUsed($request->used))],
            'file' => MediaType::ruleTypesFromUsed($request->used, $request->type),
        ])->validate();

        $used = $request->used;
        $folder = $used;
        $filename = $used;
        $type = $request->type;
        $disk = MediaType::disk($used);

        $media = $this->saveMedia($disk, $request->file('file'), $folder, $filename, $type, $used);

        return $this->sendResponseResource(new MediaResource($media));
    }

    private function saveMedia($disk, UploadedFile $uploadedFile, $folder, $filename, $type, $used, $options = []){

        $path = $this->uploadFile($disk, $uploadedFile, $folder, $filename, $type, $used, $options);

        $media = new Media();
        $media->used = $used;
        $media->type = $type;
        $media->path = $path;
        $media->disk = $disk;
        $media->owner_id = Auth::id();
        $media->save();
        return $media;
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        $path = Storage::disk($media->disk)->path($media->path);
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = \Illuminate\Support\Facades\Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }


}
