<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function sendResponseResource(JsonResource $result, $status = Response::HTTP_OK){
        return $result->response()->setStatusCode($status)
            ->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function sendResponse($result, $status = Response::HTTP_OK){
        return response()->json($result, is_null($result) ? Response::HTTP_NO_CONTENT : $status);
    }

    public function sendMessageError($errorMessage, $status = Response::HTTP_BAD_REQUEST){
        $response['message'] = $errorMessage;
        return response()->json($response, $status);
    }

    public function sendMessageErrorWithArray($errorMessage, $errors, $status = Response::HTTP_BAD_REQUEST){
        $response['message'] = $errorMessage;
        $response['errors'] = is_array($errors) ? $errors : array($errors);
        return response()->json($response, $status);
    }

    public function checkPassword($password){
        return Hash::check($password, Auth::user()->getAuthPassword());
    }
}
