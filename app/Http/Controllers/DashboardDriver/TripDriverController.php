<?php

namespace App\Http\Controllers\DashboardDriver;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardDriver\Trip\IndexTripDriverRequest;
use App\Http\Requests\DashboardDriver\Trip\PauseTripDriverRequest;
use App\Http\Requests\DashboardDriver\Trip\RefreshSolutionTripDriverRequest;
use App\Http\Requests\DashboardDriver\Trip\StartTripDriverRequest;
use App\Http\Resources\DashboardDriver\DefaultTrip\GenerateTripFromDefaultTripDriverResource;
use App\Http\Resources\DashboardDriver\Trip\TripDriverResource;
use App\Models\Company;
use App\Models\DefaultTrip;
use App\Models\Trip;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class TripDriverController extends Controller
{

    public function index(IndexTripDriverRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $tripsQuery = Auth::user()->employee->trips()->with('pointsOfSale');
        $tripsQuery->filter($validatedData);
        $trips = $tripsQuery->paginate($per_page);
        return $this->sendResponseResource(TripDriverResource::collection($trips));
    }

    public function getCurrentTrip(Company $company){
        $trip = Auth::user()->employee->trips()->with('pointsOfSale')->where('date', now()->format('Y-m-d'))->first();
        if ($trip)
            return $this->sendResponseResource(TripDriverResource::make($trip));
        return $this->sendResponse(null);
    }


    public function show(Company $company, Trip $trip){
        return $this->sendResponseResource(TripDriverResource::make($trip->load('pointsOfSale')));
    }


    public function start(StartTripDriverRequest $request, Company $company, Trip $trip){
        $validatedData = $request->validated();

        $trip->update([
            'status' => 'active'
        ]);
        $trip->refreshSolution($validatedData['current_lat'], $validatedData['current_long']);

        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }

    public function pause(PauseTripDriverRequest $request, Company $company, Trip $trip){
        $validatedData = $request->validated();

        $trip->update([
            'status' => 'paused'
        ]);

        $lastStatusHistoryStart = $trip->statusHistoryTrip()->where('status', 'start')->latest()->first();
        $trip->statusHistoryTrip()->create([
            'action_date' => now(),
            'status' => 'pause',
            'route_distance' => $lastStatusHistoryStart->route_distance,
            'route_duration' => $lastStatusHistoryStart->route_duration,
            'lat' => $validatedData['current_lat'],
            'long' => $validatedData['current_long'],
        ]);
        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }


    public function cancel(PauseTripDriverRequest $request, Company $company, Trip $trip){
        $validatedData = $request->validated();

        $trip->update([
            'status' => 'canceled'
        ]);

        $lastStatusHistoryStart = $trip->statusHistoryTrip()->where('status', 'start')->latest()->first();
        $trip->statusHistoryTrip()->create([
            'status' => 'pause',
            'action_date' => now(),
            'route_distance' => $lastStatusHistoryStart->route_distance,
            'route_duration' => $lastStatusHistoryStart->route_duration,
            'lat' => $validatedData['current_lat'],
            'long' => $validatedData['current_long'],
        ]);

        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }


    public function refreshSolution(RefreshSolutionTripDriverRequest $request, Company $company, Trip $trip){
        $validatedData = $request->validated();

        $trip->refreshPointsOfSale($validatedData['current_lat'], $validatedData['current_long']);

        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }

}
