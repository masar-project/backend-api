<?php

namespace App\Http\Controllers\DashboardDriver;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardDriver\PointOfSale\SetStatusFailurePointOfSaleTripDriverRequest;
use App\Http\Requests\DashboardDriver\PointOfSale\SetStatusIntransitPointOfSaleTripDriverRequest;
use App\Http\Requests\DashboardDriver\PointOfSale\SetStatusSuccessPointOfSaleTripDriverRequest;
use App\Http\Resources\DashboardDriver\DefaultTrip\PreviewDefaultTripDriverResource;
use App\Http\Resources\DashboardDriver\Trip\TripDriverResource;
use App\Models\Company;
use App\Models\PointOfSale;
use App\Models\Trip;

class PointOfSaleDriverController extends Controller
{
    public function setIntransit(SetStatusIntransitPointOfSaleTripDriverRequest $request, Company $company, Trip $trip, PointOfSale $pointOfSale){
        $validatedData = $request->validated();
        $trip->setPointOfSaleStatusIntransit($pointOfSale->id);

        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }

    public function setSuccess(SetStatusSuccessPointOfSaleTripDriverRequest $request, Company $company, Trip $trip, PointOfSale $pointOfSale){
        $validatedData = $request->validated();
        $trip->setPointOfSaleStatusSuccess($pointOfSale->id);
        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }

    public function setFailure(SetStatusFailurePointOfSaleTripDriverRequest $request, Company $company, Trip $trip, PointOfSale $pointOfSale){
        $validatedData = $request->validated();
        $trip->setPointOfSaleStatusFailure($pointOfSale->id,
            isset($validatedData['failure_note']) ? $validatedData['failure_note'] : null
            , $validatedData['point_of_sale_failure_reason_id']);
        return $this->sendResponseResource(TripDriverResource::make($trip->fresh('pointsOfSale')));
    }


}
