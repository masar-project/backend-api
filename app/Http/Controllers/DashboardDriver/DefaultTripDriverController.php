<?php

namespace App\Http\Controllers\DashboardDriver;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardDriver\DefaultTrip\IndexDefaultTripDriverRequest;
use App\Http\Requests\DashboardDriver\DefaultTrip\PreviewDefaultTripDriverRequest;
use App\Http\Resources\DashboardDriver\DefaultTrip\DefaultTripDriverResource;
use App\Http\Resources\DashboardDriver\DefaultTrip\GenerateTripFromDefaultTripDriverResource;
use App\Http\Resources\DashboardDriver\DefaultTrip\PreviewDefaultTripDriverResource;
use App\Models\Company;
use App\Models\DefaultTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class DefaultTripDriverController extends Controller
{

    public function index(IndexDefaultTripDriverRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $defaultTripsQuery = Auth::user()->employee->defaultTrips()->with('pointsOfSale');
        $defaultTripsQuery->filter($validatedData);
        $defaultTrips = $defaultTripsQuery->paginate($per_page);
        return $this->sendResponseResource(DefaultTripDriverResource::collection($defaultTrips));
    }

    public function show(Company $company, DefaultTrip $defaultTrip){
        return $this->sendResponseResource(DefaultTripDriverResource::make($defaultTrip->load('pointsOfSale')));
    }

    public function preview(PreviewDefaultTripDriverRequest $request, Company $company, DefaultTrip $defaultTrip){
        $validatedData = $request->validated();
        $result = $defaultTrip->getPreview($validatedData['date']);
        return $this->sendResponseResource(PreviewDefaultTripDriverResource::make($result));
    }

}
