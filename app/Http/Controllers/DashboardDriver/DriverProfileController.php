<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardCompany\MyAccount\EditProfileImgMyAccountRequest;
use App\Http\Requests\DashboardCompany\MyAccount\EditProfileMyAccountRequest;
use App\Http\Resources\DashboardCompany\Driver\DriverCompanyResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DriverProfileController extends Controller
{

    public function showMyAccount(){
        $user = Auth::user();
        $employee = $user->employee;
        return $this->sendResponseResource(DriverCompanyResource::make($employee));
    }


    public function editProfileImg(EditProfileImgMyAccountRequest $request){

        $validatedData = $request->validated();

        $user = Auth::user();
        DB::beginTransaction();
        try{

            $user->update([
                'profile_img_id' => isset($validatedData['profile_img_id'])
                    ? $request['profile_img_id']
                    : Config::get('constants.media_config.profile_img_default_'.$user->gender)
            ]);

            $employee = $user->employee;

            DB::commit();
            return $this->sendResponseResource(DriverCompanyResource::make($employee));
        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendMessageError($e->getMessage());
        }
    }

    public function editProfile(EditProfileMyAccountRequest $request){

        $validatedData = $request->validated();

        $user = Auth::user();
        DB::beginTransaction();
        try{

            $user->update([
                'first_name' => $validatedData['first_name'],
                'last_name' => $validatedData['last_name'],
                'gender' => $validatedData['gender'],
                'mobile_number' => $validatedData['mobile_number'],
                'birth_date' => $validatedData['birth_date'],
            ]);
            $employee = $user->employee;

            DB::commit();
            return $this->sendResponseResource(DriverCompanyResource::make($employee));
        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendMessageError($e->getMessage());
        }
    }
}
