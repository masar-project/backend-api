<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\PointOfSaleFailureReason;
use Illuminate\Http\Request;

class PointOfSaleFailureReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PointOfSaleFailureReason  $pointOfSaleFailureReason
     * @return \Illuminate\Http\Response
     */
    public function show(PointOfSaleFailureReason $pointOfSaleFailureReason)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PointOfSaleFailureReason  $pointOfSaleFailureReason
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointOfSaleFailureReason $pointOfSaleFailureReason)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PointOfSaleFailureReason  $pointOfSaleFailureReason
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointOfSaleFailureReason $pointOfSaleFailureReason)
    {
        //
    }
}
