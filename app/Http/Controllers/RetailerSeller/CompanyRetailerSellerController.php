<?php

namespace App\Http\Controllers\RetailerSeller;

use App\Http\Controllers\Controller;
use App\Http\Requests\RetailerSeller\Company\IndexCompanyRetailerSellerRequest;
use App\Http\Requests\RetailerSeller\Company\ShowCompanyRetailerSellerRequest;
use App\Http\Resources\Global_\Company\CompanyResource;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CompanyRetailerSellerController extends Controller
{

    public function index(IndexCompanyRetailerSellerRequest $request){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $companyQuery = Company::query();
        $companyQuery->filter($validatedData);
        $companies = $companyQuery->paginate($per_page);

        return $this->sendResponseResource(CompanyResource::collection($companies));
    }







}
