<?php

namespace App\Http\Controllers\RetailerSeller;

use App\Http\Controllers\Controller;
use App\Http\Requests\RetailerSeller\Cart\IndexCartRetailerSellerRequest;
use App\Http\Requests\RetailerSeller\Cart\OrderItemCartRetailerSellerRequest;
use App\Http\Requests\RetailerSeller\Cart\UpdateQuantityItemCartRetailerSellerRequest;
use App\Http\Resources\RetailerSeller\Cart\CartItemRetailerSellerResponse;
use App\Models\Company;
use App\Models\ItemCart;
use App\Models\RetailersSellers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CartRetailerSellerController extends Controller
{
    public function index(IndexCartRetailerSellerRequest $request, RetailersSellers $retailersSellers){
        $validatedData = $request->validated();
        $itemsCart = $retailersSellers->itemsCart()->get();

        $itemsCart = $itemsCart->groupBy('product.company_id');
        $items = [];
        foreach ($itemsCart as $itemsCompanyCart){
            $company = $itemsCompanyCart->get(0)->product->company;
            $obj['company'] = $company;
            $obj['itemsCompanyCart'] = $itemsCompanyCart;
            array_push($items, $obj);
        }

        return $this->sendResponseResource(CartItemRetailerSellerResponse::make($items));
    }

    public function updateQuantity(UpdateQuantityItemCartRetailerSellerRequest $request, RetailersSellers $retailersSellers
        , Company $company, ItemCart $itemCart)
    {
        $validatedData = $request->validated();
        $itemCart->update([
            'quantity' => $validatedData['quantity']
        ]);

        $itemsCart = $retailersSellers->itemsCart()->get();
        $itemsCart = $itemsCart->groupBy('product.company_id');
        $items = [];
        foreach ($itemsCart as $itemsCompanyCart){
            $company = $itemsCompanyCart->get(0)->product->company;
            $obj['company'] = $company;
            $obj['itemsCompanyCart'] = $itemsCompanyCart;
            array_push($items, $obj);
        }

        return $this->sendResponseResource(CartItemRetailerSellerResponse::make($items));
    }


    public function order(OrderItemCartRetailerSellerRequest $request, RetailersSellers $retailersSellers)
    {
        $validatedData = $request->validated();
        $order = $retailersSellers->order()->create([
            'company_id' => $validatedData['company_id']
        ]);

        foreach ($validatedData['item_cart_ids'] as $itemCartId){
            $itemCart = ItemCart::query()->find($itemCartId);
            $order->itemsOrder()->create([
                'product_id' => $itemCart->product_id,
                'quantity' => $itemCart->quantity,
            ]);
        }

        return $this->sendResponse(null);

    }

}
