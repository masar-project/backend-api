<?php

namespace App\Http\Controllers\retailerSeller;

use App\Http\Controllers\Controller;
use App\Http\Requests\RetailerSeller\Product\AddToCartProductRetailerSellerRequest;
use App\Http\Requests\RetailerSeller\Product\IndexProductRetailerSellerRequest;
use App\Http\Resources\RetailerSeller\Product\ProductRetailerSellerResource;
use App\Models\Company;
use App\Models\ItemCart;
use App\Models\Product;
use App\Models\RetailersSellers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ProductRetailerSellerController extends Controller
{
    public function index(IndexProductRetailerSellerRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));
        $productQuery = $company->products();
        $productQuery->filter($validatedData);
        $product = $productQuery->paginate($per_page);

        return $this->sendResponseResource(ProductRetailerSellerResource::collection($product));
    }

    public function addToCart(AddToCartProductRetailerSellerRequest $request, RetailersSellers $retailersSellers, Company $company, Product $product){
        $itemCart = $retailersSellers->itemsCart()->where('product_id', $product->id)->firstOr(function () use ($product, $retailersSellers) {
            return $retailersSellers->itemsCart()->create([
                'product_id' => $product->id,
            ]);
        })->increment('quantity');

        return $this->sendResponse(null);
    }
}

