<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\CustomizedPoint\IndexCustomizedPointCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\CustomizedPoint\StoreCustomizedPointCompanyRequest;
use App\Http\Resources\DashboardEmployee\Company\CustomizedPoint\CustomizedPointCompanyResource;
use App\Models\Company;
use App\Models\CustomizedPoint;
use Illuminate\Support\Facades\Config;

class CustomizedPointCompanyController extends Controller
{

    public function index(IndexCustomizedPointCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));
        $customizedPointQuery = $company->customizedPoints();
        $customizedPointQuery->filter($validatedData);

        $customizedPoints = $customizedPointQuery->paginate($per_page);

        return $this->sendResponseResource(CustomizedPointCompanyResource::collection($customizedPoints));
    }

    public function show(CustomizedPoint $customizedPoint, Company $company){
        return $this->sendResponseResource(CustomizedPointCompanyResource::make($customizedPoint));
    }

    public function store(StoreCustomizedPointCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $trip = $company->trips()
            ->where('date', $validatedData['date'])
            ->where('default_trip_id', $validatedData['default_trip_id'])
            ->firstOr(function (){
                return null;
            });


        if ($validatedData['type'] != 'addition' && $trip && $trip->pointsOfSale()->where('point_of_sale_trip.id', $validatedData['point_of_sale_id'])->exists()) {
            if ($trip->getPointsOfSaleWhereStatus('idle')->firstWhere('id', $validatedData['point_of_sale_id'])){
                $customizedPoint = $company->customizedPoints()->create($validatedData);
            }else{
                return $this->sendMessageError("can't delete point not idle");
            }
        }else{
            $customizedPoint = $company->customizedPoints()->create($validatedData);
        }
        return $this->sendResponseResource(CustomizedPointCompanyResource::make($customizedPoint->fresh()));
    }

    public function delete(CustomizedPoint $customizedPoint){
        //todo policy

        $customizedPoint->delete();
        return $this->sendResponse(null);
    }

}
