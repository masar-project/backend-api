<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\Trip\IndexTripCompanyRequest;
use App\Http\Resources\DashboardEmployee\Company\Trip\TripCompanyResource;
use App\Models\Company;
use App\Models\DefaultTrip;
use App\Models\Trip;
use App\Traits\GenerateSolutionTrait;
use Illuminate\Support\Facades\Config;

class TripCompanyController extends Controller
{

    use GenerateSolutionTrait;

    public function index(IndexTripCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $tripQuery = $company->trips()->with('pointsOfSale');
        $tripQuery->filter($validatedData);
        $trips = $tripQuery->paginate($per_page);
        return $this->sendResponseResource(TripCompanyResource::collection($trips));
    }



}
