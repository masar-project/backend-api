<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\DefaultTrip\GenerateTripFromDefaultTripDriverRequest;
use App\Http\Requests\DashboardEmployee\Company\DefaultTrip\IndexDefaultTripCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\DefaultTrip\PreviewDefaultTripCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\DefaultTrip\StoreDefaultTripCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\DefaultTrip\UpdateDefaultTripCompanyRequest;
use App\Http\Resources\DashboardEmployee\Company\DefaultTrip\DefaultTripCompanyResource;
use App\Http\Resources\DashboardEmployee\Company\DefaultTrip\PreviewDefaultTripCompanyResource;
use App\Models\Company;
use App\Models\DefaultTrip;
use App\Models\PointOfSale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class DefaultTripCompanyController extends Controller
{

    public function index(IndexDefaultTripCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $defaultTripsQuery = $company->defaultTrips()->with('pointsOfSale');
        $defaultTripsQuery->filter($validatedData);
        $defaultTrips = $defaultTripsQuery->paginate($per_page);

        return $this->sendResponseResource(DefaultTripCompanyResource::collection($defaultTrips));
    }

    public function store(StoreDefaultTripCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $defaultTrip = DefaultTrip::query()->create([
            'company_id' => $company->id,
            'day' => $validatedData['day'],
            'name' => $validatedData['name'],
            'driver_id' => $validatedData['driver_id'],
        ]);

        $defaultTrip->pointsOfSale()->attach($validatedData['point_of_sale_ids']);

        if (now()->format('l') == $validatedData['day']){
            $date = now();
            $previewTrip = $defaultTrip->getPreview($date);

            $trip = $defaultTrip->trips()->updateOrCreate(
                [
                    'date' => $date->format('Y-m-d')
                ],
                [
                    'driver_id' => $previewTrip['driver']['id'],
                    'name' => $defaultTrip->name,
                    'company_id' => $company->id,
                ]
            );

            $trip->refreshDriver();
            $trip->refreshPointsOfSale($company->lat, $company->long);
        }

        return $this->sendResponseResource(DefaultTripCompanyResource::make($defaultTrip->fresh('pointsOfSale')));
    }

    public function show(Company $company, DefaultTrip $defaultTrip){
        return $this->sendResponseResource(DefaultTripCompanyResource::make($defaultTrip->load('pointsOfSale')));
    }

    public function update(UpdateDefaultTripCompanyRequest $request, Company $company, DefaultTrip $defaultTrip){

        $validatedData = $request->validated();

        $defaultTrip->update([
            'name' => $validatedData['name'],
            'day' => $validatedData['day'],
            'driver_id' => $validatedData['driver_id'],
        ]);

        $defaultTrip->pointsOfSale()->detach();
        $defaultTrip->pointsOfSale()->attach($validatedData['point_of_sale_ids']);
        return $this->sendResponseResource(DefaultTripCompanyResource::make($defaultTrip->fresh('pointsOfSale')));
    }

    public function delete(DefaultTrip $defaultTrip){
        $defaultTrip->delete();
        return $this->sendResponse(null);
    }

    public function preview(PreviewDefaultTripCompanyRequest $request, Company $company, DefaultTrip $defaultTrip){
        $validatedData = $request->validated();
        $result = $defaultTrip->getPreview($validatedData['date']);
        return $this->sendResponseResource(PreviewDefaultTripCompanyResource::make($result));
    }



}
