<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\Profile\EditProfileCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Profile\EditProfileImgCompanyRequest;
use App\Http\Resources\Global_\Company\CompanyResource;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CompanyProfileController extends Controller
{

    public function showCompanyDetails(Company $company){
        return $this->sendResponseResource(CompanyResource::make($company));
    }

    public function editProfileImg(EditProfileImgCompanyRequest $request, Company $company){

        $validatedData = $request->validated();

        DB::beginTransaction();
        try{

            $company->update([
                'img_id' => isset($validatedData['img_id'])
                    ? $request['img_id']
                    : Config::get('constants.media_config.company_profile_img_default')
            ]);

            DB::commit();
            return $this->sendResponseResource(CompanyResource::make($company->fresh()));
        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendMessageError($e->getMessage());
        }
    }


    public function editProfile(EditProfileCompanyRequest $request, Company $company){

        $validatedData = $request->validated();

        DB::beginTransaction();
        try{

            $company->update([
                'name' => $validatedData['name'],
                'city_id' => $validatedData['city_id'],
                'region_id' => $validatedData['region_id'],
                'address' => $validatedData['address'],
                'lat' => $validatedData['lat'],
                'long' => $validatedData['long'],
            ]);

            DB::commit();
            return $this->sendResponseResource(CompanyResource::make($company->fresh()));
        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendMessageError($e->getMessage());
        }
    }

    public function editCategories(){

    }

}
