<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\PointOfSale\IndexPosCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\PointOfSale\StorePosCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\PointOfSale\UpdatePosCompanyRequest;
use App\Http\Resources\DashboardEmployee\Company\PointsOfSale\PointOfSaleCompanyResource;
use App\Models\Company;
use App\Models\PointOfSale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PointOfSaleCompanyController extends Controller
{

    public function index(IndexPosCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $pointsOfSaleQuery = $company->pointsOfSale();
        $pointsOfSaleQuery->filter($validatedData);
        $pointsOfSale = $pointsOfSaleQuery->paginate($per_page);

        return $this->sendResponseResource(PointOfSaleCompanyResource::collection($pointsOfSale));
    }

    public function store(StorePosCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $validatedData['work_times'] = json_encode($validatedData['work_times']);
        $validatedData['company_id'] = $company->id;
        $pointOfSale = PointOfSale::query()->create($validatedData);

        return $this->sendResponseResource(PointOfSaleCompanyResource::make($pointOfSale->fresh()));
    }



    public function update(UpdatePosCompanyRequest $request, Company $company, PointOfSale $pointOfSale){

        $validatedData = $request->validated();

        $validatedData['work_times'] = json_encode($validatedData['work_times']);
        $pointOfSale->update($validatedData);

        return $this->sendResponseResource(PointOfSaleCompanyResource::make($pointOfSale));
    }



    public function show(Company $company, PointOfSale $pointOfSale){
        return $this->sendResponseResource(PointOfSaleCompanyResource::make($pointOfSale));
    }


    public function disable(Company $company, PointOfSale $pointOfSale){
        $pointOfSale->update([
            'disable' => true,
            'action_disable_employee_id' => Auth::user()->employee->id,
        ]);
        return $this->sendResponse(null);
    }


    public function enable(Company $company, PointOfSale $pointOfSale){
        $pointOfSale->update([
            'disable' => false,
            'action_disable_employee_id' => Auth::user()->employee->id,
        ]);
        return $this->sendResponse(null);
    }



}
