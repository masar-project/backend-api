<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\Employee\IndexEmployeeCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Employee\StoreEmployeeCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Employee\UpdateEmployeeCompanyRequest;
use App\Http\Resources\DashboardEmployee\Employee\EmployeeCompanyResource;
use App\Models\Company;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class EmployeeCompanyController extends Controller
{


    public function index(IndexEmployeeCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $employeesQuery = $company->employees();
        $employeesQuery->filter($validatedData);
        $employees = $employeesQuery->paginate($per_page);

        return $this->sendResponseResource(EmployeeCompanyResource::collection($employees));
    }




    public function store(StoreEmployeeCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $profile_img_id = isset($validatedData['profile_img_id'])
            ? $request['profile_img_id']
            : Config::get('constants.media_config.profile_img_default_'.$validatedData['gender']);

        $user = User::query()->create([
            'profile_img_id' =>  $profile_img_id,
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'gender' => $validatedData['gender'],
            'mobile_number' => $validatedData['mobile_number'],
            'birth_date' => $validatedData['birth_date'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
        ]);

        $employee = $user->employee()->create([
            'company_id' =>  $company->id,
            'role' => $validatedData['role'],
        ]);

        return $this->sendResponseResource(EmployeeCompanyResource::make($employee->fresh()));
    }


    public function show(Company $company, Employee $employee){
        return $this->sendResponseResource(EmployeeCompanyResource::make($employee));
    }



    public function update(UpdateEmployeeCompanyRequest $request, Company $company, Employee $employee){

        $validatedData = $request->validated();

        $profile_img_id = isset($validatedData['profile_img_id'])
            ? $request['profile_img_id']
            : Config::get('constants.media_config.profile_img_default_'.$validatedData['gender']);

        $employee->User()->update([
            'profile_img_id' =>  $profile_img_id,
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'gender' => $validatedData['gender'],
            'mobile_number' => $validatedData['mobile_number'],
            'birth_date' => $validatedData['birth_date'],
        ]);

        return $this->sendResponseResource(EmployeeCompanyResource::make($employee->fresh()));
    }

    public function disable(Company $company, Employee $employee){
        $employee->update([
            'disable' => true,
            'action_disable_employee_id' => Auth::user()->employee->id,
        ]);
        return $this->sendResponse(null);
    }


    public function enable(Company $company, Employee $employee){
        $employee->update([
            'disable' => false,
            'action_disable_employee_id' => Auth::user()->employee->id,
        ]);
        return $this->sendResponse(null);
    }

}
