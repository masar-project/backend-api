<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\Product\IndexProductCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Product\StoreProductCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Product\UpdateProductCompanyRequest;
use App\Http\Resources\DashboardEmployee\Company\Product\ProductCompanyResource;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ProductCompanyController extends Controller
{

    public function index(IndexProductCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $productsQuery = $company->products();
        $productsQuery->filter($validatedData);
        $products = $productsQuery->paginate($per_page);

        return $this->sendResponseResource(ProductCompanyResource::collection($products));
    }


    public function store(StoreProductCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $product = $company->products()->create([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'price' => $validatedData['price'],
            'product_code' => $validatedData['product_code'],
            'category_id' => $validatedData['category_id'],
            'img_id' => $validatedData['img_id'],
        ]);

        return $this->sendResponseResource(ProductCompanyResource::make($product));
    }

    public function update(UpdateProductCompanyRequest $request, Company $company, Product $product){
        $validatedData = $request->validated();

        $product->update([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'price' => $validatedData['price'],
            'product_code' => $validatedData['product_code'],
            'category_id' => $validatedData['category_id'],
            'img_id' => $validatedData['img_id'],
        ]);

        return $this->sendResponseResource(ProductCompanyResource::make($product->fresh()));
    }

    public function delete(Request $request, Company $company, Product $product){
        $product->delete();

        return $this->sendResponse(null);
    }


}
