<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\Driver\IndexDriverCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Driver\StoreDriverCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\Driver\UpdateDriverCompanyRequest;
use App\Http\Resources\DashboardEmployee\Driver\DriverCompanyResource;
use App\Models\Company;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class DriverCompanyController extends Controller
{


    public function index(IndexDriverCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $driversQuery = $company->drivers();
        $driversQuery->filter($validatedData);
        $drivers = $driversQuery->paginate($per_page);

        return $this->sendResponseResource(DriverCompanyResource::collection($drivers));
    }




    public function store(StoreDriverCompanyRequest $request, Company $company){
        $validatedData = $request->validated();

        $profile_img_id = isset($validatedData['profile_img_id'])
            ? $request['profile_img_id']
            : Config::get('constants.media_config.profile_img_default_'.$validatedData['gender']);

        $user = User::query()->create([
            'profile_img_id' =>  $profile_img_id,
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'gender' => $validatedData['gender'],
            'mobile_number' => $validatedData['mobile_number'],
            'birth_date' => $validatedData['birth_date'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
        ]);

        $driver = $user->employee()->create([
            'company_id' =>  $company->id,
            'role' => "driver",
        ]);

        return $this->sendResponseResource(DriverCompanyResource::make($driver->fresh()));
    }




    public function show(Company $company, Employee $driver){
        return $this->sendResponseResource(DriverCompanyResource::make($driver));
    }




    public function update(UpdateDriverCompanyRequest $request, Company $company, Employee $driver){

        $validatedData = $request->validated();

        $profile_img_id = isset($validatedData['profile_img_id'])
            ? $request['profile_img_id']
            : Config::get('constants.media_config.profile_img_default_'.$validatedData['gender']);

        $driver->User()->update([
            'profile_img_id' =>  $profile_img_id,
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'gender' => $validatedData['gender'],
            'mobile_number' => $validatedData['mobile_number'],
            'birth_date' => $validatedData['birth_date'],
        ]);

        return $this->sendResponseResource(DriverCompanyResource::make($driver->fresh()));
    }


    public function disable(Company $company, Employee $driver){
        $driver->update([
            'disable' => true,
            'action_disable_employee_id' => Auth::user()->employee->id,
        ]);
        return $this->sendResponse(null);
    }


    public function enable(Company $company, Employee $driver){
        $driver->update([
            'disable' => false,
            'action_disable_employee_id' => Auth::user()->employee->id,
        ]);
        return $this->sendResponse(null);
    }

}
