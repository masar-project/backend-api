<?php

namespace App\Http\Controllers\DashboardEmployee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardEmployee\Company\CustomizedDriver\IndexCustomizedDriverCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\CustomizedDriver\StoreCustomizedDriverCompanyRequest;
use App\Http\Requests\DashboardEmployee\Company\CustomizedDriver\UpdateCustomizedDriverCompanyRequest;
use App\Http\Resources\DashboardEmployee\Company\CustomizedDriver\CustomizedDriverCompanyResource;
use App\Models\Company;
use App\Models\CustomizedDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CustomizedDriverCompanyController extends Controller
{

    public function index(IndexCustomizedDriverCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $per_page = $request->input('per_page', Config::get('constants.api_config.per_page'));

        $customizedDriverQuery = $company->customizedDrivers();
        $customizedDriverQuery->filter($validatedData);
        $customizedDrivers = $customizedDriverQuery->paginate($per_page);

        return $this->sendResponseResource(CustomizedDriverCompanyResource::collection($customizedDrivers));
    }

    public function store(StoreCustomizedDriverCompanyRequest $request, Company $company){
        $validatedData = $request->validated();
        $customizedDriver = $company->customizedDrivers()->create($validatedData);

        $trip = $company->trips()
            ->where('date', $validatedData['date'])
            ->where('default_trip_id', $validatedData['default_trip_id'])
            ->firstOr(function (){
                return null;
            });

        if ($trip){
            $trip->refreshDriver();
        }

        return $this->sendResponseResource(CustomizedDriverCompanyResource::make($customizedDriver));
    }

    public function show(Company $company, CustomizedDriver $customizedDriver){
        return $this->sendResponseResource(CustomizedDriverCompanyResource::make($customizedDriver));
    }

    public function delete(CustomizedDriver $customizedDriver){
        $customizedDriver->delete();
        return $this->sendResponse(null);
    }

}
