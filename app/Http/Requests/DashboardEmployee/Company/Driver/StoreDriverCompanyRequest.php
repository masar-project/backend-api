<?php

namespace App\Http\Requests\DashboardEmployee\Company\Driver;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreDriverCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_img_id' => [Rule::exists('media', 'id')
                ->whereIn('type', MediaType::listTypesFromUsed('profile_img'))
                ->where('used', 'profile_img')],
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'gender' => ['required',Rule::in(['male', 'female'])],
            'mobile_number' => ['required', 'string', 'max:191', 'regex:/(^(\+)([0-9]{1,3})(\s\(?([0-9]{1,3})?\)?([0-9-\-]{1,11})?)$)/u'],
            'birth_date' =>  ['required', 'date', 'before:8 years ago'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
}
