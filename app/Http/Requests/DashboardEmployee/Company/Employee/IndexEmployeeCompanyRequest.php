<?php

namespace App\Http\Requests\DashboardEmployee\Company\Employee;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class IndexEmployeeCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'disable' => [Rule::in(['true', 'false'])],
            'role' => [Rule::in(['manager', 'normal'])],
            'gender' => [Rule::in(['male', 'female'])],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
