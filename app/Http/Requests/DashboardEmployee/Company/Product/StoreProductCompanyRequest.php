<?php

namespace App\Http\Requests\DashboardEmployee\Company\Product;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreProductCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string', 'min:2', 'max:255'],
            'description' => ['string', 'min:2', 'max:255'],
            'price' => ['numeric', 'min:1', 'max:9999'],
            'product_code' => ['string', 'min:2', 'max:255'],
            'category_id' => ['required' ,Rule::exists("categories")],
            'img_id' => ['required' ,Rule::exists("media")->where('used', 'product_img')],
        ];
    }
}
