<?php

namespace App\Http\Requests\DashboardEmployee\Company\CustomizedPoint;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreCustomizedPointCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $trip = $this->company->trips()
            ->where('date', $this['date'])
            ->where('default_trip_id', $this['default_trip_id'])
            ->firstOr(function (){
                return null;
            });

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $company_id = Auth::user()->employee->company_id;
        return [
            'date' =>  ['required', 'date', ],
            'lat' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'long' => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'type' => [Rule::in(['addition', 'deletion'])],
            'point_of_sale_id' => [Rule::exists('points_of_sale', 'id')
                ->where('company_id', $company_id),
                Rule::unique('customized_points', 'point_of_sale_id')
                    ->where('default_trip_id', $this->default_trip_id)
                    ->where('date', $this->date)],
            'default_trip_id' => [Rule::exists('default_trips', 'id')
                ->where('company_id', $company_id)],
        ];
    }
}

