<?php

namespace App\Http\Requests\DashboardEmployee\Company\CustomizedPoint;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateCustomizedPointCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //todo just date not coming
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $company_id = Auth::user()->employee->company_id;
        return [
            'date' =>  ['required', 'date'],
            'type' => [Rule::in(['addition', 'deletion'])],
            'point_of_sale_id' => [Rule::exists('points_of_sale', 'id')
                ->where('company_id', $company_id),
                Rule::unique('customized_points', 'point_of_sale_id')
                    ->where('default_trip_id', $this->default_trip_id)
                    ->where('date', $this->date)],
            'default_trip_id' => [Rule::exists('default_trips', 'id')
                ->where('company_id', $company_id)],
        ];
    }
}
