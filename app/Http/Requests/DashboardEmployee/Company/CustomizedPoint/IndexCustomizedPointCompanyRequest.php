<?php

namespace App\Http\Requests\DashboardEmployee\Company\CustomizedPoint;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class IndexCustomizedPointCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company_id = Auth::user()->employee->company_id;
        return [
            'city_id' => [Rule::exists('cities', 'id')],
            'region_id' => [Rule::exists('regions', 'id')
                ->where('city_id', $this['city_id'])],
            'key_search' => ['string', 'min:2', 'max:255'],
            'date' =>  ['date'],
            'point_of_sale_id' => [Rule::exists('points_of_sale', 'id')
                ->where('company_id', $company_id)],
            'type' => [Rule::in(['addition', 'deletion'])],
            'ago_date' => ['date', 'before:today'],
        ];
    }
}
