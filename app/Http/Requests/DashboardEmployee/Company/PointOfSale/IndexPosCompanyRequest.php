<?php

namespace App\Http\Requests\DashboardEmployee\Company\PointOfSale;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class IndexPosCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'disable' => [Rule::in(['true', 'false'])],
            'city_id' => [Rule::exists('cities', 'id')],
            'region_id' => [Rule::exists('regions', 'id')
                ->where('city_id', $this['city_id'])],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
