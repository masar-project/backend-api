<?php

namespace App\Http\Requests\DashboardEmployee\Company\PointOfSale;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdatePosCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        return [
            'name' => ['required' ,'max:255', Rule::unique('points_of_sale', 'name')
                ->where('company_id', $user->employee->company_id)->ignore($this->pointOfSale->id)],
            'city_id' => ['required', Rule::exists('cities', 'id')],
            'region_id' => ['required', Rule::exists('regions', 'id')
                ->where('city_id', $this['city_id'])],
            'address' => 'required|max:2000',
            'lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'long' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],



            'work_times' => ['required'],
            'work_times.Saturday' => ['required'],
            'work_times.Saturday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Saturday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Saturday.vacation' => ['required', 'boolean'],
            'work_times.Sunday' => ['required'],
            'work_times.Sunday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Sunday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Sunday.vacation' => ['required', 'boolean'],
            'work_times.Monday' => ['required'],
            'work_times.Monday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Monday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Monday.vacation' => ['required', 'boolean'],
            'work_times.Tuesday' => ['required'],
            'work_times.Tuesday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Tuesday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Tuesday.vacation' => ['required', 'boolean'],
            'work_times.Wednesday' => ['required'],
            'work_times.Wednesday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Wednesday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Wednesday.vacation' => ['required', 'boolean'],
            'work_times.Thursday' => ['required'],
            'work_times.Thursday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Thursday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Thursday.vacation' => ['required', 'boolean'],
            'work_times.Friday' => ['required'],
            'work_times.Friday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Friday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'work_times.Friday.vacation' => ['required', 'boolean'],
        ];
    }
}
