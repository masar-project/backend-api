<?php

namespace App\Http\Requests\DashboardEmployee\Company\Profile;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditProfileImgCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'img_id' => [Rule::exists('media', 'id')
                ->whereIn('type', MediaType::listTypesFromUsed('company_profile_img'))
                ->where('used', 'company_profile_img')],
        ];
    }
}
