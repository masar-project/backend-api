<?php

namespace App\Http\Requests\DashboardEmployee\Company\Trip;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateTripCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company_id = Auth::user()->employee->company_id;
        return [
            'date' => ['date'],
            'default_trip_id' => [Rule::exists('default_trips', 'id')
                ->where('company_id', $company_id)],
            'driver_id' => [Rule::exists('employees', 'id')
                ->where('company_id', $company_id)],
            'status' => [Rule::in(['idle', 'active', 'paused', 'done'])],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
