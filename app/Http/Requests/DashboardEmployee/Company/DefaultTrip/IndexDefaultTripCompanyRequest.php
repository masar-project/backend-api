<?php

namespace App\Http\Requests\DashboardEmployee\Company\DefaultTrip;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class IndexDefaultTripCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company_id = Auth::user()->employee->company_id;
        return [
            'day' => [Rule::in(Carbon::getDays())],
            'driver_id' => [Rule::exists('employees', 'id')
                ->where('company_id', $company_id)],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
