<?php

namespace App\Http\Requests\DashboardEmployee\Company\DefaultTrip;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateDefaultTripCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company_id = Auth::user()->employee->company_id;
        return [
            'day' => ['required',Rule::in(Carbon::getDays())],
            'name' => ['required', 'string', 'max:255'],
            'driver_id' => ['required', Rule::exists('employees', 'id')
                ->where('company_id', $company_id)],
            'point_of_sale_ids' => ['required', 'array', 'distinct', 'min:1', Rule::exists('points_of_sale', 'id')
                ->where('company_id', $company_id)],
        ];
    }
}
