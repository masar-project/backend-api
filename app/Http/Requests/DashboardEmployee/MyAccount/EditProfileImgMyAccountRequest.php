<?php

namespace App\Http\Requests\DashboardEmployee\MyAccount;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditProfileImgMyAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_img_id' => [Rule::exists('media', 'id')
                ->whereIn('type', MediaType::listTypesFromUsed('profile_img'))
                ->where('used', 'profile_img')],
        ];
    }
}
