<?php

namespace App\Http\Requests\DashboardEmployee\MyAccount;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditProfileMyAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'gender' => ['required',Rule::in(['male', 'female'])],
            'mobile_number' => ['required', 'string', 'max:191', 'regex:/(^(\+)([0-9]{1,3})(\s\(?([0-9]{1,3})?\)?([0-9-\-]{1,11})?)$)/u'],
            'birth_date' =>  ['required', 'date', 'before:8 years ago'],
        ];
    }
}
