<?php

namespace App\Http\Requests\RetailerSeller\Product;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IndexProductRetailerSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => [Rule::exists('companies')],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
