<?php

namespace App\Http\Requests\RetailerSeller\Company;

use App\helper\MediaType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IndexCompanyRetailerSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => [Rule::exists('cities')],
            'region_id' => [Rule::exists('regions')],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
