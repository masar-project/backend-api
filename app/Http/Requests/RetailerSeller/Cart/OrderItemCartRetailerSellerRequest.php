<?php

namespace App\Http\Requests\RetailerSeller\Cart;

use App\helper\MediaType;
use App\Rules\CheckItemCartFromCompany;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderItemCartRetailerSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required', Rule::exists('companies')],
            'item_cart_ids' => ['required', 'array', 'min:1'],
            'item_cart_ids.*' => [Rule::exists('item_cart', 'id')
                ->where('retailer_id', $this->route('retailersSellers')->id)
                , new CheckItemCartFromCompany(request('company_id'))],
        ];
    }
}
