<?php

namespace App\Http\Requests\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'company.name' => 'required|unique:companies,name|max:255',
            'company.city_id' => ['required', Rule::exists('cities', 'id')],
            'company.region_id' => ['required', Rule::exists('regions', 'id')
                ->where('city_id', $this->company['city_id'])],
            'company.address' => 'required|max:2000',
            'company.lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'company.long' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],


            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'gender' => ['required',Rule::in(['male', 'female'])],
            'mobile_number' => ['required', 'string', 'max:191', 'regex:/(^(\+)([0-9]{1,3})(\s\(?([0-9]{1,3})?\)?([0-9-\-]{1,11})?)$)/u'],
            'birth_date' =>  ['required', 'date', 'before:8 years ago'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

}
