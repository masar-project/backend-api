<?php

namespace App\Http\Requests\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRetailerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'retailer.name' => 'required|unique:companies,name|max:255',
            'retailer.city_id' => ['required', Rule::exists('cities', 'id')],
            'retailer.region_id' => ['required', Rule::exists('regions', 'id')
                ->where('city_id', $this->company['city_id'])],
            'retailer.address' => 'required|max:2000',
            'retailer.phone_number' => 'required|max:2000',
            'retailer.lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'retailer.long' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],

            'retailer.work_times' => ['required'],
            'retailer.work_times.Saturday' => ['required'],
            'retailer.work_times.Saturday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Saturday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Saturday.vacation' => ['required', 'boolean'],
            'retailer.work_times.Sunday' => ['required'],
            'retailer.work_times.Sunday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Sunday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Sunday.vacation' => ['required', 'boolean'],
            'retailer.work_times.Monday' => ['required'],
            'retailer.work_times.Monday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Monday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Monday.vacation' => ['required', 'boolean'],
            'retailer.work_times.Tuesday' => ['required'],
            'retailer.work_times.Tuesday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Tuesday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Tuesday.vacation' => ['required', 'boolean'],
            'retailer.work_times.Wednesday' => ['required'],
            'retailer.work_times.Wednesday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Wednesday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Wednesday.vacation' => ['required', 'boolean'],
            'retailer.work_times.Thursday' => ['required'],
            'retailer.work_times.Thursday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Thursday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Thursday.vacation' => ['required', 'boolean'],
            'retailer.work_times.Friday' => ['required'],
            'retailer.work_times.Friday.open' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Friday.close' => ['required', 'multi_date_format:H:i:s,H:i'],
            'retailer.work_times.Friday.vacation' => ['required', 'boolean'],

            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'gender' => ['required',Rule::in(['male', 'female'])],
            'mobile_number' => ['required', 'string', 'max:191', 'regex:/(^(\+)([0-9]{1,3})(\s\(?([0-9]{1,3})?\)?([0-9-\-]{1,11})?)$)/u'],
            'birth_date' =>  ['required', 'date', 'before:8 years ago'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

}
