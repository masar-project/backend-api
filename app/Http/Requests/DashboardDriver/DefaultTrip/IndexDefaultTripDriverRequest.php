<?php

namespace App\Http\Requests\DashboardDriver\DefaultTrip;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class IndexDefaultTripDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'day' => [Rule::in(Carbon::getDays())],
            'key_search' => ['string', 'min:2', 'max:255'],
        ];
    }
}
