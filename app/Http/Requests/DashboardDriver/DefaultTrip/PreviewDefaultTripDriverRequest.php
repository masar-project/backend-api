<?php

namespace App\Http\Requests\DashboardDriver\DefaultTrip;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class PreviewDefaultTripDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company_id = Auth::user()->employee->company_id;
        return [
            'default_trip_id' => [Rule::exists('default_trips', 'id')
                ->where('company_id', $company_id)],
            'date' =>  ['required', 'date'],
        ];
    }
}
