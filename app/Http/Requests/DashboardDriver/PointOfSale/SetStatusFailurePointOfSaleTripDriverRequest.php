<?php

namespace App\Http\Requests\DashboardDriver\PointOfSale;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class SetStatusFailurePointOfSaleTripDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'failure_note' => ['nullable', 'string', 'max:4000'],
            'point_of_sale_failure_reason_id' => [Rule::exists('point_of_sale_failure_reasons', 'id')],
        ];
    }
}
