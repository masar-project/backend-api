<?php


namespace App\Http\ThirdPartyAPI\DistanceMatrixAi;


use App\Exceptions\APIDistanceMatrixAiException;
use App\HttpClientRequest\RequestBuilder;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;

class APIDistanceMatrixAi
{

    /**
     * @param $destinations
     * @return \Exception|GuzzleException|mixed|\Psr\Http\Message\ResponseInterface
     * @throws APIDistanceMatrixAiException
     */
    public static function getMatrix($destinations){
        $queryData = [
            'origins' => $destinations,
            'destinations' => $destinations,
            'key'=> 'EYeEgrvC57zSOSepV9uY8Db8dSRQh'
        ];

        $response = RequestBuilder::init()
            ->setQueryParam($queryData)
            ->url('https://api.distancematrix.ai/maps/api/distancematrix/json?')
            ->build()
            ->get();

        if ($response instanceof GuzzleException){
            throw new APIDistanceMatrixAiException($response->getMessage(), Response::HTTP_BAD_REQUEST);
        } else{
            $responseJson = $response->getBody()->getContents();
            $response = json_decode($responseJson);
            if ($response->status == 'OK'){
                return $response;
            }else {
                throw new APIDistanceMatrixAiException($response->error_message, Response::HTTP_BAD_REQUEST);
            }
        }
    }
}