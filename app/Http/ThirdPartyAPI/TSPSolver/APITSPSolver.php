<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 07/7/2021
 * Time: 12:03 AM
 */

namespace App\Http\ThirdPartyAPI\TSPSolver;


use App\HttpClientRequest\RequestBuilder;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\File;

class APITSPSolver
{

    public static function getSolution($distances, $durations, $timeWindows){
        $opj['distances'] = $distances;
        $opj['durations'] = $durations;
        $opj['time_windows'] = $timeWindows;
        $response = RequestBuilder::init()
            ->url('https://solutions.masar.cloud/solve/duration')
            ->setBodyJson(json_encode($opj))
            ->build()
            ->post();

        if ($response instanceof GuzzleException)
            dd(json_encode($response->getMessage()));
        else{
            $responseJson = $response->getBody()->getContents();
            $response = json_decode($responseJson, true);
            return $response;
        }
    }
}