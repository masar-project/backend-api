<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 25/12/2020
 * Time: 12:32 AM
 */

namespace App\helper;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MediaType
{



    public static function listFullUsed(){
        $media_image_disk = 'company';

        return [
            "profile_img" => [
                'disk' => $media_image_disk,
                'public' => true,
                'types' => [
                    'image' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:8192‬'],
                ],
                'cutDimensions' => [
                    'width' => 512,
                    'height' => 512,
                ],
            ],
            "company_profile_img" => [
                'disk' => $media_image_disk,
                'public' => true,
                'types' => [
                    'image' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:8192‬'],
                ],
                'cutDimensions' => [
                    'width' => 512,
                    'height' => 512,
                ],
            ],
            "company_cover_img" => [
                'disk' => $media_image_disk,
                'public' => true,
                'types' => [
                    'image' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:8192‬'],
                ],
                'cutDimensions' => [
                    'width' => 1400,
                    'height' => 500,
                ],
            ],
            "product_img" => [
                'disk' => $media_image_disk,
                'public' => true,
                'types' => [
                    'image' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:8192‬'],
                ],
                'cutDimensions' => [
                    'width' => 512,
                    'height' => 512,
                ],
            ],
        ];
    }

    public static function listUsed(){
        return array_keys(self::listFullUsed());
    }

    public static function disk($used){
        return self::listFullUsed()[$used]['disk'];
    }

    public static function ruleTypesFromUsed($used, $type){
        return isset(self::listFullUsed()[$used]['types'][$type]) ? self::listFullUsed()[$used]['types'][$type] : [];
    }

    public static function listTypesFromUsed($used){
        return isset(self::listFullUsed()[$used]['types']) ? array_keys(self::listFullUsed()[$used]['types']) : [];
    }

    public static function cutDimensions($used){
        return isset(self::listFullUsed()[$used]['cutDimensions']) ? self::listFullUsed()[$used]['cutDimensions'] : null ;
    }


}