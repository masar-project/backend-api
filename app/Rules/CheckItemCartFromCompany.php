<?php

namespace App\Rules;

use App\Models\Company;
use App\Models\ItemCart;
use Illuminate\Contracts\Validation\Rule;

class CheckItemCartFromCompany implements Rule
{

    var $company_id;

    /**
     * Create a new rule instance.
     *
     * @param $company_id
     */
    public function __construct($company_id = -1)
    {
        $this->company_id = $company_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param array $itemCartIds
     * @return bool
     */
    public function passes($attribute, $itemCartIds = [])
    {
        foreach ($itemCartIds as $itemCartId){
            $itemCart = ItemCart::query()->find($itemCartId);
            if ($this->company_id != $itemCart->product->company_id){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The item Cart Id not valid zzz.';
    }
}
