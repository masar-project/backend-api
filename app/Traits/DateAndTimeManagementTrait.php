<?php
/**
* Created by PhpStorm.
* User: Abd Alrahman
* Date: 10/18/2019
* Time: 8:26 PM
*/
namespace App\Traits;


use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

trait DateAndTimeManagementTrait
{

    public function getFullDateFormatHaveMilliseconds(){
        $now = now();
        return  Carbon::createFromFormat('Y-m-d H:i:s', $now)->format('Y-m-d-h-i-s')."_".$now->milli;
    }


    public function checkIsSameDay($timestamp1, $timestamp2){
        return $timestamp1 instanceof Carbon
            ? $timestamp1->isSameDay($timestamp2)
            : Carbon::parse($timestamp1)->isSameDay($timestamp2);
    }

    public function checkIsSameMonth($timestamp1, $timestamp2){
        return $timestamp1 instanceof Carbon
            ? $timestamp1->isSameMonth($timestamp2)
            : Carbon::parse($timestamp1)->isSameMonth($timestamp2);
    }

    public function checkDayNameIsNext($dayName, $nextDayName){
        return $this->getNextDayName($dayName) == $nextDayName;
    }

    public function checkDayNameIsPrevious($dayName, $previousDayName){
        return $this->getPreviousDayName($dayName) == $previousDayName;
    }


    /**
     * @param $date
     * @return string
     */
    public function getNameDayFromDate($date){
        return $date instanceof Carbon
            ? $date->format('l')
            : Carbon::parse($date)->format('l');
    }

    /**
     * @param $date
     * @return Carbon
     */
    public function getHourStartOfDay($date){
        return $date instanceof Carbon
            ? $date->startOfDay()
            : Carbon::parse($date)->startOfDay();
    }

    /**
     * @param $date
     * @return Carbon
     */
    public function getHourEndOfDay($date){
        return $date instanceof Carbon
            ? $date->endOfDay()
            : Carbon::parse($date)->endOfDay();
    }

    public function getDiffInMinutes($date1, $date2, $absolute = false){
        return $date1 instanceof Carbon
            ? $date1->diffInMinutes($date2, $absolute)
            : Carbon::parse($date1)->diffInMinutes($date2, $absolute);
    }

    public function checkBetween($pointDate, $date1,  $date2, $equal = true){
        return $pointDate instanceof Carbon
            ? $pointDate->between($date1, $date2, $equal)
            : Carbon::parse($pointDate)->between($date1, $date2, $equal);
    }

    /**
     * @param $hoursString
     * @param $year
     * @param $month
     * @param $day
     * @return Carbon
     */
    public function convertStringHourToFullDate($hoursString, $year, $month, $day){
        return Carbon::parse($hoursString)->year($year)->month($month)->day($day);
    }

    /**
     * @param $month
     * @return Carbon
     */
    public function getDateStartOfMonth($month){
        return Carbon::parse($month)->startOfMonth();
    }

    /**
     * @param $month
     * @return Carbon
     */
    public function getDateEndOfMonth($month){
        return Carbon::parse($month)->endOfMonth();
    }

    public function getDateBetweenRange($dateStart, $dateEnd){
        return CarbonPeriod::between($dateStart, $dateEnd);
    }

    public function getAllDateInMonth($month){
        return $this->getDateBetweenRange($this->getDateStartOfMonth($month), $this->getDateEndOfMonth($month));
    }


    use TimeZoneDateAndTimeTrait;

    public function convertWorkTimesToUTC($workTimes){
        $workTimeAfterConvertToUTC = null;

        $days = $this->getDaysInWeek();
        foreach ($days as $day){
            $workTimeAfterConvertToUTC[$day] = [];
        }
        foreach ($days as $day){
            $times = $workTimes[$day];
            foreach ($times as $time){
                $arrayOfTimesAfterConvertToUTC = $this->convertToUTCAndCutOffIfWorkTimesFromDifferentDays($time, $day);
                foreach ($arrayOfTimesAfterConvertToUTC as $timeAfterConvertToUTC){
                    array_push($workTimeAfterConvertToUTC[$timeAfterConvertToUTC['day']], $timeAfterConvertToUTC);
                }
            }
        }
        return $workTimeAfterConvertToUTC;
    }

    public function convertWorkTimesFromUTC($workTimesUTC){
        $workTimeAfterConvertFromUTC = null;

        $days = $this->getDaysInWeek();
        foreach ($days as $day){
            $workTimeAfterConvertFromUTC[$day] = [];
        }

        foreach ($days as $day){
            $times = $workTimesUTC->{$day};
            foreach ($times as $time){
                $arrayOfTimesAfterConvertFromUTC = $this->convertFromUTCAndCutOffIfWorkTimesFromDifferentDays($time, $day);
                foreach ($arrayOfTimesAfterConvertFromUTC as $timeAfterConvertFromUTC){
                    array_push($workTimeAfterConvertFromUTC[$timeAfterConvertFromUTC['day']], $timeAfterConvertFromUTC);
                }
            }
        }
        return $workTimeAfterConvertFromUTC;
    }

    public function convertToUTCAndCutOffIfWorkTimesFromDifferentDays($opjWorkTime, $day){
        $timezone = request()->header('timezone');

        $startTime = Carbon::parse($opjWorkTime['start_time'], $timezone);
        $endTime = Carbon::parse($opjWorkTime['end_time'], $timezone);

        $diffInMinutes = $this->getDiffInMinutesTimezoneToUTC();
        $startTimeUTC = self::convertToUTC($startTime);
        $endTimeUTC = self::convertToUTC($endTime);

        $isStartTimeSameDay = $this->checkIsSameDay($startTime, $startTimeUTC);
        $isEndTimeSameDay = $this->checkIsSameDay($endTime, $endTimeUTC);

        $arr = [];
        if ($diffInMinutes < 0) { //UTC is next
            if ($isStartTimeSameDay){
                $opj1['day'] = $day;
                $opj1['start_time'] = $startTimeUTC->format('H:i');
                $opj1['end_time'] = $isEndTimeSameDay
                    ? $endTimeUTC->format('H:i')
                    : $startTimeUTC->copy()->endOfDay()->format('H:i');
                array_push($arr, $opj1);

                if (! $isEndTimeSameDay){
                    $opj2['day'] = $this->getNextDayName($day);
                    $opj2['start_time'] = $endTimeUTC->copy()->startOfDay()->format('H:i');
                    $opj2['end_time'] = $endTimeUTC->format('H:i');
                    array_push($arr, $opj2);
                }
            }else {
                $opj1['day'] = $this->getNextDayName($day);
                $opj1['start_time'] = $startTimeUTC->format('H:i');
                $opj1['end_time'] = $endTimeUTC->format('H:i');
                array_push($arr, $opj1);
            }
        } else if ($diffInMinutes > 0) { // UTC is Back
            if ($isEndTimeSameDay){
                $opj1['day'] = $day;
                $opj1['start_time'] = $isStartTimeSameDay
                    ? $startTimeUTC->format('H:i')
                    : $startTimeUTC->copy()->startOfDay()->format('H:i');
                $opj1['end_time'] = $endTimeUTC->format('H:i');

                array_push($arr, $opj1);

                if (! $isStartTimeSameDay){
                    $opj2['day'] = $this->getPreviousDayName($day);
                    $opj2['start_time'] = $startTimeUTC->format('H:i');
                    $opj2['end_time'] = $startTimeUTC->copy()->endOfDay()->format('H:i');
                    array_push($arr, $opj2);
                }
            }else{
                $opj1['day'] = $this->getPreviousDayName($day);
                $opj1['start_time'] = $startTimeUTC->format('H:i');
                $opj1['end_time'] = $endTimeUTC->format('H:i');
                array_push($arr, $opj1);
            }
        } else {
            $opj1['day'] = $day;
            $opj1['start_time'] = $startTimeUTC->format('H:i');
            $opj1['end_time'] = $endTimeUTC->format('H:i');
            array_push($arr, $opj1);
        }

        return $arr;
    }

    public function getDaysInWeek(){
        return Carbon::getDays();
    }

    public function getNextDayName($dayName){
        $dayList = Carbon::getDays();
        $position = array_search($dayName, $dayList);
        $position++;
        if ($position>6)
            $position = 0;
        return $dayList[$position];
    }

    public function getPreviousDayName($dayName){
        $dayList = $this->getDaysInWeek();
        $position = array_search($dayName, $dayList);
        $position--;
        if ($position<0)
            $position = 6;
        return $dayList[$position];
    }

    public function convertFromUTCAndCutOffIfWorkTimesFromDifferentDays($opjWorkTime, $day){

        //todo test

        $timezone = request()->header('timezone');
        $startTimeUTC = Carbon::parse($opjWorkTime->{'start_time'});
        $endTimeUTC = Carbon::parse($opjWorkTime->{'end_time'});

        $diffInMinutes = $this->getDiffInMinutesTimezoneToUTC() * -1;
        $startTime = self::convertFromUTC($startTimeUTC);
        $endTime = self::convertFromUTC($endTimeUTC);

        $isStartTimeSameDay = $this->checkIsSameDay($startTimeUTC, $startTime);
        $isEndTimeSameDay = $this->checkIsSameDay($endTimeUTC, $endTime);

        $arr = [];
        if ($diffInMinutes < 0) { //UTC is next
            if ($isStartTimeSameDay){
                $opj1['day'] = $day;
                $opj1['start_time'] = $startTime->format('H:i');
                $opj1['end_time'] = $isEndTimeSameDay
                    ? $endTime->format('H:i')
                    : $startTime->copy()->endOfDay()->format('H:i');
                array_push($arr, $opj1);

                if (! $isEndTimeSameDay){
                    $opj2['day'] = $this->getNextDayName($day);
                    $opj2['start_time'] = $endTime->copy()->startOfDay()->format('H:i');
                    $opj2['end_time'] = $endTime->format('H:i');
                    array_push($arr, $opj2);
                }
            }else {
                $opj1['day'] = $this->getNextDayName($day);
                $opj1['start_time'] = $startTime->format('H:i');
                $opj1['end_time'] = $endTime->format('H:i');
                array_push($arr, $opj1);
            }
        } else if ($diffInMinutes > 0) { // UTC is Back
            if ($isEndTimeSameDay){
                $opj1['day'] = $day;
                $opj1['start_time'] = $isStartTimeSameDay
                    ? $startTime->format('H:i')
                    : $startTime->copy()->startOfDay()->format('H:i');
                $opj1['end_time'] = $endTime->format('H:i');

                array_push($arr, $opj1);

                if (! $isStartTimeSameDay){
                    $opj2['day'] = $this->getPreviousDayName($day);
                    $opj2['start_time'] = $startTime->format('H:i');
                    $opj2['end_time'] = $startTime->copy()->endOfDay()->format('H:i');
                    array_push($arr, $opj2);
                }
            }else{
                $opj1['day'] = $this->getPreviousDayName($day);
                $opj1['start_time'] = $startTime->format('H:i');
                $opj1['end_time'] = $endTime->format('H:i');
                array_push($arr, $opj1);
            }
        } else {
            $opj1['day'] = $day;
            $opj1['start_time'] = $startTime->format('H:i');
            $opj1['end_time'] = $endTime->format('H:i');
            array_push($arr, $opj1);
        }

        return $arr;
    }


}
