<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use function PHPUnit\Framework\isEmpty;


trait SplitAndMergeTrait
{

    private $delimiter = '#@-@#';

    public function split($string){
       if (empty($string) || is_null($string))
            return [];
        return explode($this->delimiter, $string);
    }

    public function mergeArray(array $array){
        return implode($this->delimiter, $array);
    }


}