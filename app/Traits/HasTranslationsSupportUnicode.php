<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 22/6/2021
 * Time: 11:49 PM
 */

namespace App\Traits;


use Spatie\Translatable\HasTranslations;

trait HasTranslationsSupportUnicode
{
    use HasTranslations;

    /**
     * Encode the given value as JSON.
     *
     * @param  mixed  $value
     * @return string
     */
    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}