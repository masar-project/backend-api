<?php

namespace App\Traits;


use App\Http\ThirdPartyAPI\DistanceMatrixAi\APIDistanceMatrixAi;
use App\Http\ThirdPartyAPI\TSPSolver\APITSPSolver;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

trait GenerateSolutionTrait
{

    public function generateSolution($currentLat, $currentLong, $pointsOfSale){

        $pointsOfSaleReindexing = [];
        foreach ($pointsOfSale as $pointOfSale)
            array_push($pointsOfSaleReindexing, $pointOfSale);

        $pointsOfSale = $pointsOfSaleReindexing;
        $resultMatrix = $this->generateMatrix($currentLat, $currentLong, $pointsOfSale);
        $timeWindowMatrix = $this->createWorkWindow($pointsOfSale);
        $resultTSP = APITSPSolver::getSolution($resultMatrix['distances'], $resultMatrix['durations'], $timeWindowMatrix);

        //remove first and last item because start locations
        $resultTSP['locations'] = array_slice($resultTSP['locations'], 1, -1);

        $resultTSP['sortedResult'] = [];
        $resultTSP['dropped_nodes_points'] = [];
        foreach ($resultTSP['locations'] as $index){
            array_push($resultTSP['sortedResult'], $pointsOfSale[$index-1]);
        }
        foreach ($resultTSP['dropped_nodes'] as $index){
            array_push($resultTSP['dropped_nodes_points'], $pointsOfSale[$index-1]);
        }
        return $resultTSP;
    }


    //******* Matrix *********
    public function generateMatrix($currentLat, $currentLong, $pointsOfSale){
        $destinations = $this->buildStringLatLong($currentLat, $currentLong, $pointsOfSale);
        $response = APIDistanceMatrixAi::getMatrix($destinations);
        $result = $this->convertResultDistanceMatrixAiToMatrix($response->rows);
        $result['destination_addresses'] = $response->destination_addresses;
        return $result;
    }

    public function convertResultDistanceMatrixAiToMatrix($rows){
        $result['distances'] = [];
        $result['durations'] = [];
        foreach ($rows as $row) {
            $rowDistance = [];
            $rowDuration = [];
            foreach ($row->elements as $element) {
                array_push($rowDistance, $element->status == 'OK' ? $element->distance->value : 0);
                array_push($rowDuration, $element->duration->value);
            }
            array_push($result['distances'], $rowDistance);
            array_push($result['durations'], $rowDuration);
        }
        return $result;
    }

    public function buildStringLatLong($currentLat, $currentLong, $pointsOfSale){
        $destinations = $currentLat.",".$currentLong;
        foreach ($pointsOfSale as $item){
            $destinations.="|";
            $destinations.=$item->lat;
            $destinations.=",";
            $destinations.=$item->long;
        }
        return $destinations;
    }


    //******* Work Window *********
    public function createWorkWindow($pointsOfSale){
        $time_windows = [];
        foreach ($pointsOfSale as $pointOfSale){
            $workTimes = json_decode($pointOfSale->work_times);
            $workTime = $workTimes->{now()->format('l')};
            $open = Carbon::make($workTime->open);
            $close = Carbon::make($workTime->close);
            $openSecond = now()->diffInSeconds($open, false);
            $closeSecond = now()->diffInSeconds($close, false);
            array_push($time_windows, [$openSecond < 0 ? 0 : $openSecond, $closeSecond < 0 ? 0 : $closeSecond]);
        }
        return $time_windows;
    }

}