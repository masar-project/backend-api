<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use App\helper\MediaType;
use App\Models\FileSession;
use App\Models\Media;
use App\Models\Session;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


trait FileUploadSearchTutorTrait
{
    use FileManagementTrait;


    public function uploadFile($disk, UploadedFile $uploadedFile, $folder, $filename, $type, $used, $options = []){

        $filename = $this->createFileNameUnique($filename, $uploadedFile);

        if ($type == 'image')
            $uploadedFile = $this->cropImage($uploadedFile, $used);

        $path = $this->uploadFile_($disk, $uploadedFile, $folder, $filename, $options);
        return $path;
    }


    public function uploadFileSession(UploadedFile $uploadedFile, $name, $user_id){
        $disk = Config::get('constants.disks.file_session');

        $filename = $this->createFileNameUnique('file_session_'.$this->sanitizeFilename($name), $uploadedFile);

        $path = $this->uploadFile_($disk, $uploadedFile, $user_id, $filename);

        return $path;
    }


    public function sanitizeFilename($inputName) {
        $inputName =  preg_replace("([^a-zA-Z0-9-_\\.])", "_", $inputName);
        return Str::substr($inputName,0, 40);
    }
}