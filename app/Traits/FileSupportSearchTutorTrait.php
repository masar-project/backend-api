<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use App\helper\MediaType;
use App\Models\FileSession;
use App\Models\Media;
use App\Models\Session;
use App\Models\User;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


trait FileSupportSearchTutorTrait
{
    use FileManagementTrait;

    var $support_disk = 'support';

    private function createOrUpdateContentFile($folder, $filename , $content, $options = []){
        return $this->createFile_($this->support_disk, $folder."/".$filename.'.json', $content, $options);
    }

    /**
     * @param $folder
     * @param $filename
     * @return array
     * @throws FileNotFoundException
     */
    private function getContentFile($folder, $filename){
        $json = $this->getContentFile_($this->support_disk, $folder . "/" . $filename . '.json');
        return json_decode($json, true);
    }


    private function checkFileExists($folder, $filename){
        return $this->checkFileExists_($this->support_disk, $folder . "/" . $filename . '.json');
    }


    /**
     * @param $filename
     * @return array
     * @throws FileNotFoundException
     */
    public function loadMassages($filename){
        $folder = Config::get('constants.support.folder_support_messages');
        if ($this->checkFileExists($folder, $filename)){
            return $this->getContentFile($folder, $filename);
        }
        return [];
    }

    /**
     * @param $filename
     * @param $msg
     * @param array $options
     * @return string
     * @throws FileNotFoundException
     */
    public function storeMsg($filename ,$msg , $options = []){
        $folder = Config::get('constants.support.folder_support_messages');
        if ($this->checkFileExists($folder, $filename)){
            $conversationContent = $this->getContentFile($folder, $filename);
            if (is_null($conversationContent) || empty($conversationContent)){
                $conversationContent = [];
            }
            array_push($conversationContent, $msg);
            $content = json_encode($conversationContent);
        }else {
            $content = json_encode(array($msg));
        }
        return $this->createOrUpdateContentFile($folder, $filename, $content, $options);
    }
}