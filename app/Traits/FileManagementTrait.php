<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use App\helper\MediaType;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


trait FileManagementTrait
{
    use DateAndTimeManagementTrait;

    public function uploadFile_($disk, $uploadedFile, $folder, $filename, $options = []){
//        $folder = now()->year.'/'.$folder; todo add year?
        $path = Storage::disk($disk)->putFileAs($folder, $uploadedFile, $filename, $options);
        return $path;
    }


    public function createFile_($disk, $path, $content, $options = []){
        $path = Storage::disk($disk)->put($path, $content, $options);
        return $path;
    }

    /**
     * @param string $disk
     * @param string $path
     * @return string
     * @throws FileNotFoundException
     */
    public function getContentFile_($disk, $path){
        $content = Storage::disk($disk)->get($path);
        return $content;
    }

    /**
     * @param string $disk
     * @param string $path
     * @return boolean
     */
    public function checkFileExists_($disk, $path){
        return Storage::disk($disk)->exists($path);
    }



    public function createFileNameUnique($filename, UploadedFile $uploadedFile){
         return $filename."_".$this->getFullDateFormatHaveMilliseconds().".".$uploadedFile->getClientOriginalExtension();
    }



    public function cropImage($img, $used){
        $cutDimensions = MediaType::cutDimensions($used);
        if (is_null($cutDimensions))
            return $img;

        Storage::disk('public')->makeDirectory('temp');

        $tempName = $this->getFullDateFormatHaveMilliseconds()."_".Str::random(5).'.png';

        $img = Image::make($img)->fit($cutDimensions['width'], $cutDimensions['height'])->encode('png')
            ->save(storage_path('app/public/temp').'/'.$tempName);

        return new File($img->dirname.'/'.$img->basename);
    }


}