<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use App\helper\MediaType;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


trait DeleteFileTrait
{

    public function deleteFile_($disk, $path){
        $status = false;
        if (Storage::disk($disk)->exists($path))
            $status = Storage::disk($disk)->delete($path);
        return $status;
    }



}