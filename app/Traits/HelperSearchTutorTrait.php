<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use App\helper\MediaType;
use App\Models\FileSession;
use App\Models\Media;
use App\Models\Session;
use App\Models\User;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


trait HelperSearchTutorTrait
{

    /**
     * @param $array
     * @return Collection
     */
    public function convertArrayToCollection($array){
        $collection = Collection::make();
        foreach ($array as $item){
            $collection->push($item);
        }
        return $collection;
    }

    public function sortCollection(Collection $collection, $column){
        return $collection->sortBy(function ($obj, $key) use ($column) {
            return $obj[$column];
        });
    }

    public function getItemsByKeyFromCollection(Collection $collection, $key, $value){
        return $collection->where($key, $value)->toArray();
    }

    public function getItemsBetweenTowValueFromCollection(Collection $collection, $key, $value1, $value2){
       return $collection->whereBetween($key, [$value1, $value2])->toArray();
    }

    public function findInArray($array, $column, $val){
        $i = array_search($val, array_column($array, $column));
        return ($i !== false ? $array[$i] : null);
    }

}