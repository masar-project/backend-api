<?php

namespace App\Traits;
use App\Exceptions\CanNotCreateMeetingException;
use App\Exceptions\TeacherNotStartedMeetingException;
use App\Models\Meet;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Illuminate\Http\Response;
use JoisarJignesh\Bigbluebutton\Facades\Bigbluebutton;


trait MeetingManagementTrait
{

    /**
     * @param Meet $meet
     * @param $username
     * @param $userId
     * @return mixed
     * @throws CanNotCreateMeetingException
     */
    public function joinTeacher(Meet $meet, $username, $userId){

        $meetingId = $meet->id;
        $sessionTitle = $meet->session->name;
        $maxParticipants = $meet->getUserCountInMeeting() + 1 ;// +1 for admin
        if (! $this->checkMeetingIsRun($meetingId)){
            $status = $this->create($meetingId, $sessionTitle, $maxParticipants);
            if (!$status){
                throw new CanNotCreateMeetingException("can't create Meeting", Response::HTTP_BAD_REQUEST);
            }
        }

        $join = new JoinMeetingParameters($meetingId, $username, 'teacher');
        $join->setUserId($userId);
        $join->setRedirect(true);
        $join->setCustomParameter('userdata-bbb_custom_style_url', 'https://ms.searchtutor.co/customStyles2.css');
        $join->setCustomParameter('guest', 'false');
        return Bigbluebutton::join($join);
    }


    /**
     * @param Meet $meet
     * @param $username
     * @param $userId
     * @return mixed
     * @throws TeacherNotStartedMeetingException
     */
    public function joinStudent(Meet $meet, $username, $userId){

        $meetingId = $meet->id;
        if (! $this->checkMeetingIsRun($meetingId)){

            throw new TeacherNotStartedMeetingException("Teacher Not Started Meeting", Response::HTTP_BAD_REQUEST);
        }

        $join = new JoinMeetingParameters($meetingId, $username, 'student');
        $join->setUserId($userId);
        $join->setRedirect(true);
        $join->setCustomParameter('userdata-bbb_custom_style_url', 'https://ms.searchtutor.co/customStyles2.css');
        $join->setCustomParameter('guest', 'false');
        return Bigbluebutton::join($join);
    }




    private function create($meetingId, $titleSession, $maxParticipants){
        $meeting = new CreateMeetingParameters($meetingId, $titleSession);
        $meeting->setAttendeePassword("student")
            ->setModeratorPassword("teacher")
            ->setMaxParticipants($maxParticipants)
            ->setLogoutUrl('http://searchtutor.co')
            ->setRecord(true)
            ->setDuration(60)
            ->setLogo('https://ms.searchtutor.co/logo.png');

        return Bigbluebutton::create($meeting)['returncode'] == 'SUCCESS';
    }


    private function checkMeetingIsRun($meetingId){
        $meetingDetails = Bigbluebutton::getMeetingInfo([
            'meetingID' => $meetingId,
        ]);

        return sizeof($meetingDetails) == 0;
    }


}