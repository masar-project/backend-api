<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 08/11/2020
 * Time: 5:03 PM
 */

namespace App\Traits;


use App\Events\UserCompletedApplication;
use App\Notifications\ApprovedTeacherNotification;
use App\Notifications\CompleteApplicationNotification;
use App\Notifications\ReWriteApplicationNotification;

trait TeacherHelperTraits
{

    public function hasCompletedApplication()
    {
        return ! is_null($this->completed_application_at);
    }

    public function markAccountAsCompletedApplication()
    {
        $this->forceFill([
            'completed_application_at' => $this->freshTimestamp(),
        ])->save();
    }

    public function unMarkAccountAsCompletedApplication()
    {
        $this->forceFill([
            'completed_application_at' => null,
        ])->save();
    }

    public function isTeacher(){
        return $this->has('teacher');
    }

    public function sendAlertEmailBecauseTeacherCompleteApplication(){
        $this->notify(new CompleteApplicationNotification());
    }

    public function sendEmailReWriteApplication($rejectionReason){
        $this->notify(new ReWriteApplicationNotification($rejectionReason));
    }

    public function sendEmailApprovedTeacher(){
        $this->notify(new ApprovedTeacherNotification());
    }


}