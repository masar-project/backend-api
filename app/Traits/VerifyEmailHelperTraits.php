<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 08/11/2020
 * Time: 5:03 PM
 */

namespace App\Traits;


use App\Notifications\VerifyEmailNotification;
use Illuminate\Support\Str;

trait VerifyEmailHelperTraits
{

    public function sendEmailVerificationNotification(){
        $this->verification_token = Str::random(32);
        $this->save();
        return $this->notify(new VerifyEmailNotification()); // my notification
    }

}