<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/5/2020
 * Time: 8:51 PM
 */
namespace App\Traits;
use App\helper\MediaType;
use App\Models\FileSession;
use App\Models\Media;
use App\Models\Session;
use App\Models\User;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


trait DateAndTimeHelperSearchTutorTrait
{
    use HelperSearchTutorTrait;
    use TimeZoneDateAndTimeTrait;

    public function convertMeetingTimesToUTC($meetingTimes){
        $meetingTimesUTC = [];
        foreach ($meetingTimes as $meetingTime){
            $obj['session_id'] = $meetingTime['session_id'];
            $obj['meeting_start_date'] = self::convertToUTC($meetingTime['meeting_start_date'], 'Y-m-d H:i');
            array_push($meetingTimesUTC, $obj);
        }
        return $meetingTimesUTC;
    }



    public function margeWorkTimeIfOverlapping($workTimes){
        $days = $this->getDaysInWeek();
        foreach ($days as $day){
            $collectionTimes = $this->convertArrayToCollection($workTimes[$day]);

            $sorted = $collectionTimes->sortBy(function ($obj, $key) {
                return $obj['start_time'];
            });

            $compactRange = Collection::make();
            foreach ($sorted as $item){
                if ($compactRange->isEmpty()){
                    $compactRange->push($item);
                }else{
                    $lastRange = $compactRange->pop();
                    if (Carbon::parse($item['start_time'])->isBefore(Carbon::parse($lastRange['end_time'])->addMinutes(2))){//todo
                        $lastRange['end_time'] = $item['end_time'];
                        $compactRange->push($lastRange);
                    }else{
                        $compactRange->push($lastRange);
                        $compactRange->push($item);
                    }
                }
            }
            $workTimes[$day] = $compactRange->toArray();
        }
        return $workTimes;
    }

    /**
     * @param $excludeRanges {Carbon start_date, Carbon emd_date}
     * @param Carbon $originalRangeStart
     * @param Carbon $originalRangeEnd
     * @return array
     */
    public function excludeRangeFromRanges($originalRangeStart, $originalRangeEnd, $excludeRanges)
    {
        if (!$originalRangeStart instanceof Carbon)
            $originalRangeStart = Carbon::parse($originalRangeStart);

        if (!$originalRangeEnd instanceof Carbon)
            $originalRangeEnd = Carbon::parse($originalRangeEnd);

        $newRanges = [];
        if (count($excludeRanges) == 0) {
            $newRange = new \stdClass();
            $newRange->start_date = $originalRangeStart->toDateTimeString();
            $newRange->end_date = $originalRangeEnd->toDateTimeString();
            $newRanges[] = $newRange;
            return $newRanges;
        }

        $splitCount = 0;

        foreach ($excludeRanges as $excludeRange) {
            $cancelRangeStart = Carbon::parse($excludeRange['start_date']);
            $cancelRangeEnd = Carbon::parse($excludeRange['end_date']);

            $startDiffInMinutes = $this->getDiffInMinutes($originalRangeStart, $cancelRangeStart);
            if ($startDiffInMinutes >= 60 && $this->checkBetween($cancelRangeStart, $originalRangeStart, $originalRangeEnd)) {
                $newRange = new \stdClass();
                $newRange->start_date = $originalRangeStart->toDateTimeString();
                $newRange->end_date = $originalRangeStart->addMinutes($startDiffInMinutes)->toDateTimeString();
                $newRanges[] = $newRange;
                $splitCount++;
            }


            $endDiffInMinutes = $this->getDiffInMinutes($cancelRangeEnd, $originalRangeEnd);
            if ($endDiffInMinutes >= 60 && $cancelRangeEnd->between($originalRangeStart, $originalRangeEnd)) {
                $newRange = new \stdClass();
                $newRange->start_date = $cancelRangeEnd->toDateTimeString();
                $newRange->end_date = $cancelRangeEnd->addMinutes($endDiffInMinutes)->toDateTimeString();
                $newRanges[] = $newRange;
                $splitCount++;
            }

            if ($this->checkBetween($originalRangeStart, $cancelRangeStart, $cancelRangeEnd)
                && $this->checkBetween($originalRangeEnd, $cancelRangeStart, $cancelRangeEnd))
            {
                $splitCount++;
            }
        }

        if ($splitCount == 0) {
            $newRange = new \stdClass();
            $newRange->start_date = $originalRangeStart->toDateTimeString();
            $newRange->end_date = $originalRangeEnd->toDateTimeString();
            $newRanges[] = $newRange;
        }
        return $newRanges;
    }

    public function addAttributeEndDateToAllObjectInArray($array){
        foreach ($array as $item){
            $item->end_date = Carbon::parse($item->start_date)->addMinutes(60);
        }
        return $array;
    }


}