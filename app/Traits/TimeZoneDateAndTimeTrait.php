<?php
/**
* Created by PhpStorm.
* User: Abd Alrahman
* Date: 10/18/2019
* Time: 8:26 PM
*/
namespace App\Traits;


use Illuminate\Support\Carbon;

trait TimeZoneDateAndTimeTrait
{

    /**
     * @param $timestamp
     * @param null $specialFormat
     * @return Carbon|string
     */
    public static function convertToUTC($timestamp, $specialFormat = null){
        $timezone = request()->header('timezone');
        $timestamp = Carbon::parse($timestamp , $timezone)
            ->setTimezone("UTC");

        return is_null($specialFormat)
            ? $timestamp
            : $timestamp->format($specialFormat);
    }

    /**
     * @param $timestamp
     * @param null $specialFormat
     * @return Carbon|string
     */
    public static function convertFromUTC($timestamp, $specialFormat = null){
        $timezone = request()->header('timezone');
        $timestamp = Carbon::parse($timestamp , "UTC")
            ->setTimezone($timezone);

        return is_null($specialFormat)
            ? $timestamp
            : $timestamp->format($specialFormat);
    }

    public function getDiffInMinutesTimezoneToUTC(){
        $timezone = request()->header('timezone');
        $now = Carbon::now()->toDateTimeString();
        return Carbon::parse($now, $timezone)->diffInMinutes(now(), false);
    }


//
//    public static function convertToUTC($timestamp, $timezone, $justTime = false, $specialFormat = null){
//        $format = $justTime ? 'H:i:s' : 'Y-m-d H:i:s';
//        if (!is_null($specialFormat))
//            $format = $specialFormat;
//
//        return Carbon::parse($timestamp , $timezone)
//            ->setTimezone("UTC")
//            ->format($format);
//    }
//
//    public static function convertFromUTC($timestamp, $timezone, $justTime = false, $specialFormat = null){
//        $format = $justTime ? 'H:i:s' : 'Y-m-d H:i:s';
//        if (!is_null($specialFormat))
//            $format = $specialFormat;
//
//        return Carbon::parse($timestamp , "UTC")
//            ->setTimezone($timezone)
//            ->format($format);
//    }

}
