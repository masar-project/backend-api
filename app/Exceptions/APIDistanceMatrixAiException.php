<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class APIDistanceMatrixAiException extends Exception
{

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return $this->sendMessageError($this->message, $this->code);
    }

    public function sendMessageError($errorMessage, $status = Response::HTTP_BAD_REQUEST){
        $response['message'] = $errorMessage;
        return response()->json($response, $status);
    }

}
