<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 28/12/2020
 * Time: 8:28 PM
 */

namespace App\HttpClientRequest;



class RequestBuilder
{

    private $queryData;
    private $headers;
    private $bodyJson;
    private $url;


    public static function init(){
        return new self();
    }

    public function setQueryParam($queryData){
        $this->queryData = $queryData;
        return $this;
    }

    public function setHeaders($headers = []){
        $this->headers = $headers;
        return $this;
    }

    public function setBodyJson($bodyJson = []){
        $this->bodyJson = $bodyJson;
        return $this;
    }

    public function url($url){
        $this->url = $url;
        return $this;
    }

    public function build(){
        $options['headers'] = $this->headers;
        $options['body'] = $this->bodyJson;
        if (! is_null($this->queryData) && !empty($this->queryData))
            $this->url .= http_build_query($this->queryData);

        //todo exep
        //check url exist
        return new HttpClient($this->url, $options);
    }


}