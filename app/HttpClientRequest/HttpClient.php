<?php
/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 28/12/2020
 * Time: 8:10 PM
 */

namespace App\HttpClientRequest;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class HttpClient
{
    private $url;
    private $options;
    private $client;

    /**
     * HttpClient constructor.
     * @param $url
     * @param $options
     */
    public function __construct($url, $options = [])
    {
        $this->url = $url;
        $this->options = $options;
        $this->client = new Client();
    }

    public function get(){
        try {
            return $this->client->request('get', $this->url, $this->options);
        } catch (GuzzleException $e) {
            return $e;
        }
    }

    public function post(){
        try {
            return $this->client->request('post', $this->url, $this->options);
        } catch (GuzzleException $e) {
            return $e;
        }
    }

    public function put(){
        try {
            return $this->client->request('put', $this->url, $this->options);
        } catch (GuzzleException $e) {
            return $e;
        }
    }

    public function patch(){
        try {
            return $this->client->request('patch', $this->url, $this->options);
        } catch (GuzzleException $e) {
            return $e;
        }
    }

    public function delete(){
        try {
            return $this->client->request('delete', $this->url, $this->options);
        } catch (GuzzleException $e) {
            return $e;
        }
    }



}