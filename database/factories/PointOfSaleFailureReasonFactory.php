<?php

namespace Database\Factories;

use App\Models\PointOfSaleFailureReason;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointOfSaleFailureReasonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PointOfSaleFailureReason::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
