<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class SuperAdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            'email' =>  Config::get('constants.user_config.email_super_admin'),
            'password' => bcrypt('123123123'),
            'first_name' => "Super Admin",
            'last_name' => "Super Admin",
            'gender' => "male",
            'birth_date' => now()->subYears(25),
            'mobile_number' => "+963 123456789",
            'email_verified_at' => now(),
        ];

        User::query()->firstOrCreate(
            [
                'email' => $admins['email']
            ],
            $admins
        );
    }
}
