<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class employeeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminsEmployee = [
            'user_id' => 1,
            'company_id' => 1,
            'role' => "owner",
        ];

        Employee::query()->firstOrCreate(
            $adminsEmployee
        );




        $driverEmployee = [
            'user_id' => 2,
            'company_id' => 1,
            'role' => "driver",
        ];

        Employee::query()->firstOrCreate(
            $driverEmployee
        );




        $driverEmployee = [
            'user_id' => 3,
            'company_id' => 1,
            'role' => "driver",
        ];

        Employee::query()->firstOrCreate(
            $driverEmployee
        );


        $driverEmployee = [
            'user_id' => 4,
            'company_id' => 1,
            'role' => "driver",
        ];

        Employee::query()->firstOrCreate(
            $driverEmployee
        );
    }
}
