<?php

namespace Database\Seeders;

use App\Models\PointOfSale;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class PointOfSaleSeed extends Seeder
{
    private $default_work_times = '{
        "Friday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        },
        "Monday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        },
        "Saturday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        },
        "Sunday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        },
        "Thursday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        },
        "Tuesday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        },
        "Wednesday": {
            "close": "24:00",
            "open": "06:00",
            "vacation": false
        }
    }';


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(storage_path().'/assets/points_of_sale.json');
        $items = json_decode($json);

        $i = 0;
        foreach ($items as $item) {
            $item = (array) $item;
            $item['work_times'] = $this->default_work_times;
            if ($i == 4)
                $item['note'] = 'هذا المحل لا تقرضه';
            if ($i == 7)
                $item['note'] = 'لا يسحب بضاعة كثيرا';
            PointOfSale::query()->firstOrCreate($item);
            $i++;
        }
    }
}
