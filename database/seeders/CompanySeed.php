<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = [
            "img_id" => 3,
            "name" => "MTN",
            "city_id" => 1,
            "region_id" => 33,
            "address" => "test - test",
            "lat" => "33.5085568",
            "long" => "36.2774528"
        ];

        Company::query()->firstOrCreate(
            $company
        );
    }
}
