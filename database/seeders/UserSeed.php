<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminsEmail =  Config::get('constants.user_config.email_super_admin');

        User::query()->where('email', $adminsEmail)->update(
            [
                'profile_img_id' => Config::get('constants.media_config.profile_img_default_male'),
            ]
        );

        $driver = [
            'profile_img_id' => Config::get('constants.media_config.profile_img_default_male'),
            'email' =>  'driver@driver.com',
            'password' => bcrypt('123123123'),
            'first_name' => "mr.driver 1",
            'last_name' => "kk",
            'gender' => "female",
            'birth_date' => now()->subYears(25),
            'mobile_number' => "+963 123456789",
            'email_verified_at' => now(),
        ];

        User::query()->firstOrCreate(
            [
                'email' => $driver['email']
            ],
            $driver
        );


        $driver2 = [
            'profile_img_id' => Config::get('constants.media_config.profile_img_default_male'),
            'email' =>  'driver2@driver.com',
            'password' => bcrypt('123123123'),
            'first_name' => "mr.driver 2",
            'last_name' => "kk",
            'gender' => "female",
            'birth_date' => now()->subYears(25),
            'mobile_number' => "+963 123456789",
            'email_verified_at' => now(),
        ];

        User::query()->firstOrCreate(
            [
                'email' => $driver2['email']
            ],
            $driver2
        );



        $driver3 = [
            'profile_img_id' => Config::get('constants.media_config.profile_img_default_male'),
            'email' =>  'driver3@driver.com',
            'password' => bcrypt('123123123'),
            'first_name' => "mr.driver 3",
            'last_name' => "kk",
            'gender' => "female",
            'birth_date' => now()->subYears(25),
            'mobile_number' => "+963 123456789",
            'email_verified_at' => now(),
        ];

        User::query()->firstOrCreate(
            [
                'email' => $driver3['email']
            ],
            $driver3
        );
    }
}
