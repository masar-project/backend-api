<?php

namespace Database\Seeders;

use App\Models\Media;
use Illuminate\Database\Seeder;

class MediaSeed extends Seeder
{
    protected $media = [
        [
            'type' => 'image',
            'path' => '/images/profile_img_default_male.jpg',
            'owner_id' => 1,
            'used' => 'profile_img',
            'disk' => 'media_default',
        ],
        [
            'type' => 'image',
            'path' => '/images/profile_img_default_female.jpg',
            'owner_id' => 1,
            'used' => 'profile_img',
            'disk' => 'media_default',
        ],
        [
            'type' => 'image',
            'path' => '/images/company_img_default.png',
            'owner_id' => 1,
            'used' => 'company_profile_img',
            'disk' => 'media_default',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->media as $m){
            Media::query()->firstOrCreate($m);
        }
    }
}
