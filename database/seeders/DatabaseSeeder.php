<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitySeed::class);
        $this->call(RegionSeed::class);
        $this->call(SuperAdminSeed::class);
        $this->call(MediaSeed::class);
        $this->call(UserSeed::class);
        $this->call(CompanySeed::class);
        $this->call(employeeSeed::class);
        $this->call(PointOfSaleSeed::class);
        $this->call(DefaultTripSeed::class);
        $this->call(PointOfSaleFailureReasonSeeder::class);

        // \App\Models\User::factory(10)->create();
    }
}
