<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class RegionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json = File::get(storage_path().'/assets/regions.json');
        $regions = json_decode($json);

        foreach ($regions as $region){
            $item = [
                'name' => [
                    'en' => $region->name_en,
                    'ar' => $region->name_ar
                ],
                'city_id' => 1
            ];

            $json_val = json_encode($item['name'], JSON_UNESCAPED_UNICODE);
            if (! Region::query()->where('name', $json_val)->exists())
                Region::query()->firstOrCreate($item);
        }
    }
}
