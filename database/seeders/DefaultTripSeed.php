<?php

namespace Database\Seeders;

use App\Models\DefaultTrip;
use Illuminate\Database\Seeder;

class DefaultTripSeed extends Seeder
{

    public $items = [
        [
            'day' => 'Saturday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],
        [
            'day' => 'Sunday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],
        [
            'day' => 'Monday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],
        [
            'day' => 'Tuesday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],
        [
            'day' => 'Wednesday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],
        [
            'day' => 'Thursday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],
        [
            'day' => 'Friday',
            'name' => 'جولة الجسر الأبيض',
            'company_id' => 1,
            'driver_id' => 2,
        ],[
            'day' => 'Saturday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
        [
            'day' => 'Sunday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
        [
            'day' => 'Monday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
        [
            'day' => 'Tuesday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
        [
            'day' => 'Wednesday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
        [
            'day' => 'Thursday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
        [
            'day' => 'Friday',
            'name' => 'جولة المالكي',
            'company_id' => 1,
            'driver_id' => 3,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i1 = [8,36,37];
        $i2 = [9,10,11,12,14];
        $i = 0;
        foreach ($this->items as $item){
            $defaultTrip = DefaultTrip::query()->firstOrCreate($item);
//            $defaultTrip->pointsOfSale()->attach($i1);
            $defaultTrip->pointsOfSale()->attach($i > 7 ? $i2 : $i1);
            $i++;
        }

    }
}
