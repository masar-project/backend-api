<?php

namespace Database\Seeders;

use App\Models\PointOfSaleFailureReason;
use Illuminate\Database\Seeder;

class PointOfSaleFailureReasonSeeder extends Seeder
{

    public $items = [
        [
            'reason' => 'نفذ الوقود'
        ],
        [
            'reason' => 'المحل مفلق بغير العادة'
        ],
        [
            'reason' => 'وصلت متاخرا'
        ],
        [
            'reason' => 'نفذت البضاعة'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items as $item){
            PointOfSaleFailureReason::query()->firstOrCreate($item);
        }
    }
}
