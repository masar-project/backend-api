<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{


    private $items = [
        [
            'name' => [
                'en' => 'Dietetics',
                'ar' => 'غذائيات'
            ]
        ],
        [
            'name' => [
                'en' => 'Detergents',
                'ar' => 'منظفات'
            ]
        ],
        [
            'name' => [
                'en' => 'Health',
                'ar' => 'صحة'
            ]
        ]
    ];



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items as $item){
            $json_val = json_encode($item['name'], JSON_UNESCAPED_UNICODE);
            if (! Category::query()->where('name', $json_val)->exists())
                Category::query()->firstOrCreate($item);
        }
    }
}
