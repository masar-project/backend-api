<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CitySeed extends Seeder
{

    private $items = [
        [
            'name' => [
                'en' => 'Damascus',
                'ar' => 'دمشق'
            ]
        ],
        [
            'name' => [
                'en' => 'Aleppo',
                'ar' => 'حلب'
            ]
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->items as $item){
            $json_val = json_encode($item['name'], JSON_UNESCAPED_UNICODE);
            if (! City::query()->where('name', $json_val)->exists())
                City::query()->firstOrCreate($item);
        }
    }
}
