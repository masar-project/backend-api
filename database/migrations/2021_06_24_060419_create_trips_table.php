<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->date('date');
            $table->string('name');
            $table->foreignId('driver_id')->constrained('employees')->cascadeOnDelete();
            $table->foreignId('company_id')->constrained('companies')->cascadeOnDelete();
            $table->foreignId('default_trip_id')->constrained('default_trips')->cascadeOnDelete();
            $table->bigInteger('route_distance')->nullable();
            $table->bigInteger('route_duration')->nullable();
            $table->dateTime('start_at')->nullable();
            $table->dateTime('done_at')->nullable();
            $table->enum('status', ['idle', 'active', 'paused', 'done', 'canceled'])->default('idle');//todo تسكر اذا ما حدا ساوا شي
            $table->timestamps();//todo cancel
            $table->unique(array('date', 'default_trip_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
