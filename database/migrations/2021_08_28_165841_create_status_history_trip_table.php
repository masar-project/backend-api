<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusHistoryTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_history_trip', function (Blueprint $table) {
            $table->id();
            $table->foreignId('trip_id')->constrained('trips')->cascadeOnDelete();
            $table->enum('status', ['start', 'pause', 'cancel']);
            $table->dateTime('action_date');
            $table->bigInteger('route_distance')->nullable();
            $table->bigInteger('route_duration')->nullable();
            $table->decimal('lat', 10, 8);
            $table->decimal('long', 10, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_pause_start');
    }
}
