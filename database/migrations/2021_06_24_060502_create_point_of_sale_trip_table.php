<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointOfSaleTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_of_sale_trip', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('trip_id')->constrained('trips')->cascadeOnDelete();
            $table->foreignId('point_of_sale_id')->constrained('points_of_sale')->cascadeOnDelete();
            $table->foreignId('point_of_sale_failure_reason_id')->nullable()->constrained('point_of_sale_failure_reasons')->cascadeOnDelete();
            $table->integer('default_order');
            $table->integer('actual_order')->nullable();
            $table->dateTime('arrival_time')->nullable();
            $table->enum('status', ['idle', 'intransit', 'success', 'failure', 'dropped'])->default('idle');
            $table->text('failure_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_of_sale_trip');
    }
}
